package ids.task_51;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;


public class StartActivity extends ActionBarActivity {

    private final String DATA_VIEW_TYPE = "DataViewType";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void displayDataInListView (View view) {
        Intent listViewIntent = new Intent(this, DataDisplayActivity.class);
        listViewIntent.putExtra(DATA_VIEW_TYPE, "listView");
        startActivity(listViewIntent);
    }

    public void displayDataInGridView (View view) {
        Intent gridViewIntent = new Intent(this, DataDisplayActivity.class);
        gridViewIntent.putExtra(DATA_VIEW_TYPE, "gridView");
        startActivity(gridViewIntent);
    }

    public void displayDataInSpinner (View view) {
        Intent spinnerIntent = new Intent(this, DataDisplayActivity.class);
        spinnerIntent.putExtra(DATA_VIEW_TYPE, "spinner");
        startActivity(spinnerIntent);
    }


    public void displayDataInListActivity (View view) {
        Intent listActivityIntent = new Intent(this, CitiesInListActivity.class);
        startActivity(listActivityIntent);
    }

    public void displayDataInListFragment (View view) {
        Intent listFragmentIntent = new Intent(this, DataDisplayActivity.class);
        listFragmentIntent.putExtra(DATA_VIEW_TYPE, "listFragment");
        startActivity(listFragmentIntent);
    }

    public void displayDataInExpandableListView (View view) {
        Intent expListViewIntent = new Intent(this, DataDisplayActivity.class);
        expListViewIntent.putExtra(DATA_VIEW_TYPE, "expandable");
        startActivity(expListViewIntent);
    }

}
