package ids.task_51;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DataDisplayActivity extends ActionBarActivity {

    private final String DATA_VIEW_TYPE = "DataViewType";
    private String[] largestCities = { "New York", "Los Angeles", "Chicago", "Houston", "Philadelphia",
                                                "Phoenix", "San Antonio", "San Diego", "Dallas", "San Jose" };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String dataViewType = getIntent().getStringExtra(DATA_VIEW_TYPE);
        switch (dataViewType) {
            case "listView":
                setContentView(R.layout.layout_list_view);
                displayInListView();
                break;
            case "gridView":
                setContentView(R.layout.layout_grid_view);
                displayInGridView();
                break;
            case "spinner":
                setContentView(R.layout.layout_spinner);
                displayInSpinner();
                break;
            case "listFragment":
                setContentView(R.layout.layout_list_fragment);
                break;
            case "expandable":
                setContentView(R.layout.layout_expandable);
                displayInExpandableListView();
                break;
        }

    }

    private void displayInListView() {
        ListView citiesList = (ListView) findViewById(R.id.listCities);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, largestCities);
        citiesList.setAdapter(adapter);
    }

    private void displayInGridView() {
        GridView citiesGrid = (GridView) findViewById(R.id.gridCities);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, largestCities);
        citiesGrid.setAdapter(adapter);
    }

    private void displayInSpinner() {
        Spinner citiesSpinner = (Spinner) findViewById(R.id.spinnerCities);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, largestCities);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citiesSpinner.setAdapter(adapter);
    }

    private void displayInExpandableListView() {
        final String CITY_CATEGORY = "categoryName";
        final String CITY_NAME = "cityName";

        ExpandableListView citiesExpand = (ExpandableListView) findViewById(R.id.citiesExpandable);
        String[] groups = new String[] {"Over 5 mln", "2-5 mln", "1-2 mln", "Less than 1 mln"};

        String[] over5mln = new String[] {"New York"};
        String[] over2mln = new String[] {"Los Angeles", "Chicago", "Houston"};
        String[] over1mln= new String[] {"Philadelphia", "Phoenix", "San Antonio", "San Diego", "Dallas"};
        String[] lessThan1Mln= new String[] {"San Jose"};

        // groups
        ArrayList<Map<String, String>> groupData;

        // one group's elements
        ArrayList<Map<String, String>> childDataItem;

        // final collection with data
        ArrayList<ArrayList<Map<String, String>>> childData;

        Map<String, String> m;

        groupData = new ArrayList<>();
        for (String group : groups) {
            m = new HashMap<>();
            m.put(CITY_CATEGORY, group); // city category
            groupData.add(m);
        }

        childData = new ArrayList<>();

        // adding elements to each city category (group)
        childDataItem = new ArrayList<>();

        for (String category : over5mln) {
            m = new HashMap<>();
            m.put(CITY_NAME, category);
            childDataItem.add(m);
        }
        childData.add(childDataItem);

        childDataItem = new ArrayList<>();
        for (String category : over2mln) {
            m = new HashMap<>();
            m.put(CITY_NAME, category);
            childDataItem.add(m);
        }
        childData.add(childDataItem);

        childDataItem = new ArrayList<>();
        for (String category : over1mln) {
            m = new HashMap<>();
            m.put(CITY_NAME, category);
            childDataItem.add(m);
        }
        childData.add(childDataItem);

        childDataItem = new ArrayList<>();
        for (String category : lessThan1Mln) {
            m = new HashMap<>();
            m.put(CITY_NAME, category);
            childDataItem.add(m);
        }
        childData.add(childDataItem);

        SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(
                this, groupData, android.R.layout.simple_expandable_list_item_1,
                new String[] { CITY_CATEGORY },new int[] { android.R.id.text1 }, childData,
                android.R.layout.simple_list_item_1, new String[] { CITY_NAME }, new int[] { android.R.id.text1 });

        citiesExpand.setAdapter(adapter);
    }

}
