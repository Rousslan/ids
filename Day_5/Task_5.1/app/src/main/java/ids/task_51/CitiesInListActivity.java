package ids.task_51;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class CitiesInListActivity extends ListActivity {

    private String[] largestCities = { "New York", "Los Angeles", "Chicago", "Houston", "Philadelphia",
            "Phoenix", "San Antonio", "San Diego", "Dallas", "San Jose" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, largestCities);
        setListAdapter(adapter);
    }

}
