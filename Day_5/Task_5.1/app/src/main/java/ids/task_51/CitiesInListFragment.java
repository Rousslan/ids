package ids.task_51;

import android.app.ListFragment;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class CitiesInListFragment extends ListFragment{

    private String[] largestCities = { "New York", "Los Angeles", "Chicago", "Houston", "Philadelphia",
            "Phoenix", "San Antonio", "San Diego", "Dallas", "San Jose" };

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, largestCities);
        setListAdapter(adapter);
    }
}
