package ids.task_52;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

import ids.task_52.pojo.City;
import ids.task_52.pojo.CityCategory;


public class StartActivity extends ActionBarActivity {

    private final String DATA_VIEW_TYPE = "DataViewType";
    private final String PASS_CITIES_VIA_INTENT = "passCities";
    private final String PASS_CITIES_CATEGORY_VIA_INTENT = "passCitiesCategory";


    private ArrayList<City> cities;
    private ArrayList<CityCategory> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        initCities();
    }

    private void initCities() {
        cities = new ArrayList<>();
        cities.add(new City("New York", "NY", "8.4 mln"));
        cities.add(new City("Los Angeles", "CA", "3.9 mln"));
        cities.add(new City("Chicago", "IL", "2.7 mln"));
        cities.add(new City("Houston", "TX", "2.2 mln"));
        cities.add(new City("Philadelphia", "PA", "1.6 mln"));
        cities.add(new City("Phoenix", "AZ", "1.5 mln"));
        cities.add(new City("San Antonio", "TX", "1.4 mln"));
        cities.add(new City("San Diego", "CA", "1.4 mln"));
        cities.add(new City("Dallas", "TX", "1.3 mln"));
        cities.add(new City("San Jose", "CA", "1.0 mln"));
    }

    public void displayDataInListView (View view) {
        Intent listViewIntent = new Intent(this, DataDisplayActivity.class);
        listViewIntent.putExtra(PASS_CITIES_VIA_INTENT, cities);
        listViewIntent.putExtra(DATA_VIEW_TYPE, "listView");
        startActivity(listViewIntent);
    }

    public void displayDataInGridView (View view) {
        Intent gridViewIntent = new Intent(this, DataDisplayActivity.class);
        gridViewIntent.putExtra(PASS_CITIES_VIA_INTENT, cities);
        gridViewIntent.putExtra(DATA_VIEW_TYPE, "gridView");
        startActivity(gridViewIntent);
    }

    public void displayDataInSpinner (View view) {
        Intent spinnerViewIntent = new Intent(this, DataDisplayActivity.class);
        spinnerViewIntent.putExtra(PASS_CITIES_VIA_INTENT, cities);
        spinnerViewIntent.putExtra(DATA_VIEW_TYPE, "spinner");
        startActivity(spinnerViewIntent);
    }

    public void displayDataInExpandableListView (View view) {
        initCategories();

        Intent expListViewViewIntent = new Intent(this, DataDisplayActivity.class);
        expListViewViewIntent.putExtra(PASS_CITIES_CATEGORY_VIA_INTENT, categories);
        expListViewViewIntent.putExtra(DATA_VIEW_TYPE, "expandable");
        startActivity(expListViewViewIntent);
    }

    private void initCategories() {
        final String OVER_5_MLN = "Over 5 mln";
        final String FROM_2_TO_5_MLN = "2 - 5 mln";
        final String FROM_1_TO_2_MLN = "1 - 2 mln";
        final String LESS_THAN_1_MLN = "Less than 1 mln";

        /*categories' initialization code is pretty
         *straightforward for simplicity's sake*/
        categories = new ArrayList<>();

        //creating category "Over 5 mln"
        ArrayList<City> citiesCategorized = new ArrayList<>();
        citiesCategorized.add(cities.get(0));
        categories.add(new CityCategory(OVER_5_MLN, citiesCategorized));

        //creating category "2 - 5 mln"
        citiesCategorized = new ArrayList<>();
        citiesCategorized.add(cities.get(1));
        citiesCategorized.add(cities.get(2));
        citiesCategorized.add(cities.get(3));
        categories.add(new CityCategory(FROM_2_TO_5_MLN, citiesCategorized));

        //creating category "1 - 2 mln"
        citiesCategorized = new ArrayList<>();
        citiesCategorized.add(cities.get(4));
        citiesCategorized.add(cities.get(5));
        citiesCategorized.add(cities.get(6));
        citiesCategorized.add(cities.get(7));
        citiesCategorized.add(cities.get(8));
        categories.add(new CityCategory(FROM_1_TO_2_MLN, citiesCategorized));

        //creating category "Less than 1 mln"
        citiesCategorized = new ArrayList<>();
        citiesCategorized.add(cities.get(9));
        categories.add(new CityCategory(LESS_THAN_1_MLN, citiesCategorized));
    }

}
