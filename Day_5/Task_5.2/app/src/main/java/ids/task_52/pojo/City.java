package ids.task_52.pojo;

import java.io.Serializable;

public class City implements Serializable{

    private String cityName;
    private String state;
    private String population;

    public City(String cityName, String state, String population) {
        this.cityName = cityName;
        this.state = state;
        this.population = population;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

}
