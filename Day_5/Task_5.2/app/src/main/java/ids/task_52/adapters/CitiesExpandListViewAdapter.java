package ids.task_52.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ids.task_52.R;
import ids.task_52.pojo.City;
import ids.task_52.pojo.CityCategory;

public class CitiesExpandListViewAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<CityCategory> categories;

    public CitiesExpandListViewAdapter(Context context, ArrayList<CityCategory> categories) {
        this.context = context;
        this.categories= categories;
    }

    @Override
    public int getGroupCount() {
        return categories.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return categories.get(i).getCities().size();
    }

    @Override
    public Object getGroup(int i) {
        return categories.get(i);
    }

    @Override
    public Object getChild(int i, int i2) {
        return categories.get(i).getCities().get(i2);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i2) {
        return i2;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        CityCategory cityCategory = (CityCategory) getGroup(i);

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.city_list_item, null);
        }
        String categoryName = cityCategory.getCityCategoryName();
        ((TextView) view.findViewById(R.id.cityItem)).setText(categoryName);
        return view;
    }

    @Override
    public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup) {

        City city = (City) getChild(i, i2);

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.city_exp_list_item, null);
        }

        String cityItem = city.getCityName() + ", " + city.getState() + ", " + city.getPopulation();
        ((TextView) view.findViewById(R.id.cityItem)).setText(cityItem);
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return false;
    }
}
