package ids.task_52.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import ids.task_52.R;
import ids.task_52.pojo.City;

public class CitiesListViewAdapter extends CitiesAdapter{


    public CitiesListViewAdapter(List<City> cities, Context context) {
        super(cities, context);
    }

    @Override
    protected View initView(ViewGroup viewGroup, View view) {
        if (view == null) {
            view = getInflater().inflate(R.layout.city_list_item, viewGroup, false);
        }
        return view;
    }
}
