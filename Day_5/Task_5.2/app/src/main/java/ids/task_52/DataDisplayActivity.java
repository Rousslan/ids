package ids.task_52;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import ids.task_52.adapters.CitiesExpandListViewAdapter;
import ids.task_52.adapters.CitiesGridViewAdapter;
import ids.task_52.adapters.CitiesListViewAdapter;
import ids.task_52.adapters.CitiesSpinnerAdapter;
import ids.task_52.pojo.City;
import ids.task_52.pojo.CityCategory;

public class DataDisplayActivity extends ActionBarActivity {

    private final String DATA_VIEW_TYPE = "DataViewType";
    private final String PASS_CITIES_VIA_INTENT = "passCities";
    private final String PASS_CITIES_CATEGORY_VIA_INTENT = "passCitiesCategory";

    private List<City> cities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String dataViewType = getIntent().getStringExtra(DATA_VIEW_TYPE);
        cities = (List<City>) getIntent().getSerializableExtra(PASS_CITIES_VIA_INTENT);
        switch (dataViewType) {
            case "listView":
                setContentView(R.layout.layout_list_view);
                displayInListView();
                break;
            case "gridView":
                setContentView(R.layout.layout_grid_view);
                displayInGridView();
                break;
            case "spinner":
                setContentView(R.layout.layout_spinner_view);
                displayInSpinner();
                break;
            case "expandable":
                setContentView(R.layout.layout_expandable);
                displayInExpandableListView();
                break;
        }
    }

    private void displayInListView() {
        ListView citiesList = (ListView) findViewById(R.id.listCities);
        CitiesListViewAdapter adapter = new CitiesListViewAdapter(cities, this);
        citiesList.setAdapter(adapter);
    }

    private void displayInGridView() {
        GridView citiesGrid = (GridView) findViewById(R.id.gridCities);
        CitiesGridViewAdapter adapter = new CitiesGridViewAdapter(cities, this);
        citiesGrid.setAdapter(adapter);
    }

    private void displayInSpinner() {
        Spinner citiesSpinner = (Spinner) findViewById(R.id.spinnerCities);
        CitiesSpinnerAdapter adapter = new CitiesSpinnerAdapter(cities, this);
        citiesSpinner.setAdapter(adapter);
    }

    private void displayInExpandableListView() {
        ExpandableListView citiesExpListView = (ExpandableListView) findViewById(R.id.citiesExpandable);
        ArrayList<CityCategory> categories = (ArrayList<CityCategory>) getIntent().getSerializableExtra(PASS_CITIES_CATEGORY_VIA_INTENT);
        ExpandableListAdapter adapter = new CitiesExpandListViewAdapter(this, categories);
        citiesExpListView.setAdapter(adapter);
    }

}
