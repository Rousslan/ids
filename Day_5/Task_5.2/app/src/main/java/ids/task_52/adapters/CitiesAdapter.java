package ids.task_52.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import ids.task_52.R;
import ids.task_52.pojo.City;

public abstract class CitiesAdapter extends BaseAdapter{

    private List<City> cities;
    private Context context;

    public LayoutInflater getInflater() {
        return inflater;
    }

    private LayoutInflater inflater;

    protected CitiesAdapter(List<City> cities, Context context) {
        this.cities = cities;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return cities.size();
    }

    @Override
    public Object getItem(int i) {
        return cities.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        view = initView(viewGroup, view);

        City city = (City) getItem(i);
        String cityItem = city.getCityName() + ", " + city.getState() + ", " + city.getPopulation();
        ((TextView) view.findViewById(R.id.cityItem)).setText(cityItem);
        return view;
    }
    /*the view is provided by the concrete subclass adapter*/
    protected abstract View initView(ViewGroup viewGroup, View view);
}
