package ids.task_52.pojo;

import java.io.Serializable;
import java.util.List;

public class CityCategory implements Serializable{

    private String cityCategoryName;
    private List<City> cities;

    public CityCategory(String cityCategoryName, List<City> cities) {
        this.cityCategoryName = cityCategoryName;
        this.cities = cities;
    }

    public String getCityCategoryName() {
        return cityCategoryName;
    }

    public void setCityCategoryName(String cityCategoryName) {
        this.cityCategoryName = cityCategoryName;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
