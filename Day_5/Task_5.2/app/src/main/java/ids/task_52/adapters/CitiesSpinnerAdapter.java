package ids.task_52.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import ids.task_52.R;
import ids.task_52.pojo.City;

public class CitiesSpinnerAdapter extends CitiesAdapter{

    public CitiesSpinnerAdapter(List<City> cities, Context context) {
        super(cities, context);
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = super.getView(i, convertView, viewGroup);
        ((TextView) view.findViewById(R.id.place)).setText((getItemId(i) + 1) + "");
        return view;
    }

    @Override
    protected View initView(ViewGroup viewGroup, View view) {
        if (view == null) {
            view = getInflater().inflate(R.layout.city_spinner_item, viewGroup, false);
        }
        return view;
    }
}
