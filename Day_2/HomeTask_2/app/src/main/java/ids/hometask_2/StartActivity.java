package ids.hometask_2;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class StartActivity extends ActionBarActivity {

    private EditText textToBroadcast;
    private Button startBroadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        textToBroadcast = (EditText) findViewById(R.id.textToBroadcast);
        startBroadcast = (Button) findViewById(R.id.startBroadcast);

        startBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("ids.task_2.3.ManifestRegisteredBroadcast");
                broadcastIntent.putExtra("textToBroadcast", textToBroadcast.getText().toString());
                sendBroadcast(broadcastIntent);
            }
        });
    }
}
