package ids.task_23;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class TextBroadcastReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        String textToBroadcast = intent.getStringExtra("textToBroadcast");
        Toast.makeText(context, "Message received from StartActivity's intent: " + textToBroadcast, Toast.LENGTH_LONG).show();
    }
}
