package ids.task_23;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class StartActivity extends ActionBarActivity {

    private EditText textToBroadcast;
    private Button startBroadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        textToBroadcast = (EditText) findViewById(R.id.textToBroadcast);
        startBroadcast = (Button) findViewById(R.id.startBroadcast);

        startBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("ids.task_2.3.CodeRegisteredBroadcast");
                broadcastIntent.putExtra("textToBroadcast", textToBroadcast.getText().toString());

                IntentFilter intentFilter = new IntentFilter("ids.task_2.3.CodeRegisteredBroadcast");
                TextBroadcastReceiver receiver = new TextBroadcastReceiver();
                registerReceiver(receiver, intentFilter);

                sendBroadcast(broadcastIntent);
            }
        });
    }
}
