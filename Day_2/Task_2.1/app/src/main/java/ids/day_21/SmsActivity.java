package ids.day_21;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ids.day_21.R;

public class SmsActivity extends ActionBarActivity {

    private EditText phoneNumber;
    private EditText body;
    private Button sendSMS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        body = (EditText) findViewById(R.id.smsBody);
        sendSMS = (Button) findViewById(R.id.sendSMS);

        sendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("smsto:" + phoneNumber.getText().toString());
                Intent smsSIntent = new Intent(Intent.ACTION_SENDTO, uri);
                smsSIntent.putExtra("sms_body", body.getText().toString());
                startActivity(smsSIntent);
            }
        });
    }

}
