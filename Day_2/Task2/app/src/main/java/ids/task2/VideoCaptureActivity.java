package ids.task2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;

public class VideoCaptureActivity extends ActionBarActivity {

    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 1;
    private Uri fileUri;
    private VideoView video;
    private Button capture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_capture);

        video = (VideoView) findViewById(R.id.video);
        capture = (Button) findViewById(R.id.capture);

        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                File videoFolder = new File(Environment.getExternalStorageDirectory(), "MyVideo");
                videoFolder.mkdirs();
                File videoFile = new File(videoFolder, "video_001.mp4");
                fileUri = Uri.fromFile(videoFile);
                videoIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                videoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                startActivityForResult(videoIntent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Video saved to:\n" +
                        data.getData(), Toast.LENGTH_LONG).show();
                video.setVideoURI(Uri.parse(fileUri.toString()));
                video.setMediaController(new MediaController(this));
                video.requestFocus();
                video.start();
            }
        }
    }

}
