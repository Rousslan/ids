package ids.task2;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class StartActivity extends ActionBarActivity {

    private Button contact;
    private Button image;
    private Button camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        contact = (Button) findViewById(R.id.contact);
        image = (Button) findViewById(R.id.image);
        camera = (Button) findViewById(R.id.camera);

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contactChooserIntent = new Intent(getApplicationContext(), ContactChooserActivity.class);
                startActivity(contactChooserIntent);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent imageChooserIntent = new Intent(getApplicationContext(), ImageChooserActivity.class);
                startActivity(imageChooserIntent);
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent videoChooserIntent = new Intent(getApplicationContext(), VideoCaptureActivity.class);
                startActivity(videoChooserIntent);
            }
        });
    }


}
