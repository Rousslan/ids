package ids.task2;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ContactChooserActivity extends ActionBarActivity {

    private Button browse;
    private EditText email;
    static final int PICK_CONTACT=1;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_chooser);

        browse = (Button) findViewById(R.id.email_picker);
        email = (EditText) findViewById(R.id.email);

        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    String id = contactData.getLastPathSegment();
                    Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?", new String[] { id },
                            null);
                    if (cursor.moveToFirst()) {
                        int emailIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);
                        String emailGot = cursor.getString(emailIndex);
                        email.setText(emailGot);
                    }
                    cursor.close();
                }
                break;
        }
    }

}
