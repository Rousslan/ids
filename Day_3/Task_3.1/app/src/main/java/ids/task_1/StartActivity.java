package ids.task_1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class StartActivity extends ActionBarActivity {

    private EditText numForFact;
    private Button calculate;
    private TextView factResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        numForFact = (EditText) findViewById(R.id.numberForFactorial);
        calculate = (Button) findViewById(R.id.calc);
        factResult = (TextView) findViewById(R.id.fact);

        calculate.setOnClickListener(new FactorialCalculator());
    }

    private class FactorialCalculator implements Runnable, View.OnClickListener {

        private int number;
        private int factorial;

        @Override
        public void run() {
            factorial = 1;
            for (int i = number; i > 1; i--) {
                factorial *= i;
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    factResult.setText("Result: " + number + "! = " + factorial);
                }
            });
        }

        @Override
        public void onClick(View view) {
            try {
                number = Integer.valueOf(numForFact.getText().toString());
            }catch (NumberFormatException ex) {
                factResult.setText("");
                Toast wrongInputToast = Toast.makeText(getApplicationContext(),
                        "Enter a numeric value",
                        Toast.LENGTH_SHORT);
                wrongInputToast.setGravity(Gravity.CENTER, 0, 0);
                wrongInputToast.show();
                return;
            }
            Thread calcThread = new Thread(this);
            calcThread.start();
        }
    }

}
