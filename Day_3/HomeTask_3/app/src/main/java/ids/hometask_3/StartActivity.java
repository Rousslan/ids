package ids.hometask_3;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;


public class StartActivity extends ActionBarActivity {

    private Button emulationStart;
    private static TextView processNotifier;
    private LongProcessEmulationTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        emulationStart = (Button) findViewById(R.id.emulationStart);
        processNotifier = (TextView) findViewById(R.id.procNotifier);

        emulationStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                task = (LongProcessEmulationTask) getLastNonConfigurationInstance();
                if (task == null) {
                    task = new LongProcessEmulationTask();
                    task.execute();
                }
                task.linkToActivity(StartActivity.this);
            }
        });
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        task.destroyLinkToActivity();
        return task;
    }

    private static class LongProcessEmulationTask extends AsyncTask<Void, Void, Void> {

        StartActivity startActivity;

        void linkToActivity(StartActivity startActivity) {
            this.startActivity = startActivity;
        }

        void destroyLinkToActivity() {
            this.startActivity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            processNotifier.setText("Long process emulation started...");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            processNotifier.setText("Long process emulation is finished.");
        }
    }

}
