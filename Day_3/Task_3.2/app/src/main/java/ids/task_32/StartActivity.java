package ids.task_32;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class StartActivity extends ActionBarActivity {

    private EditText numForFact;
    private Button calculate;
    private TextView factResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        numForFact = (EditText) findViewById(R.id.numberForFactorial);
        calculate = (Button) findViewById(R.id.calc);
        factResult = (TextView) findViewById(R.id.fact);

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FactorialCalculator factCalc = new FactorialCalculator();
                try {
                    factCalc.execute();
                }catch (NumberFormatException ex) {
                    factResult.setText("");
                    Toast wrongInputToast = Toast.makeText(getApplicationContext(),
                            "Enter a numeric value",
                            Toast.LENGTH_SHORT);
                    wrongInputToast.setGravity(Gravity.CENTER, 0, 0);
                    wrongInputToast.show();
                }
            }
        });
    }

    private class FactorialCalculator extends AsyncTask<Void, Void, Integer> {

        private int number;
        private int factorial;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            number = Integer.valueOf(numForFact.getText().toString());
        }

        @Override
        protected Integer doInBackground(Void... integers) {
            factorial = 1;
            for (int i = number; i > 1; i--) {
                factorial *= i;
            }
            return factorial;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            factResult.setText("Result: " + number + "! = " + factorial);
        }
    }

}
