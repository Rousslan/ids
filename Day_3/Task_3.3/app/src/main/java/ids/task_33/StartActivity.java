package ids.task_33;

import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class StartActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Integer> {

    private static final int LOADER_ID = 1;

    private EditText numForFact;
    private Button calculate;
    private TextView factResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        numForFact = (EditText) findViewById(R.id.numberForFactorial);
        calculate = (Button) findViewById(R.id.calc);
        factResult = (TextView) findViewById(R.id.fact);

        Bundle bundle = new Bundle();
        bundle.putInt("number", Integer.valueOf(numForFact.getText().toString()));
        getLoaderManager().initLoader(LOADER_ID, bundle, this).forceLoad();
    }

    @Override
    public Loader<Integer> onCreateLoader(int i, Bundle bundle) {
        int number = bundle.getInt("number");
        FactorialCalculator factCalc = new FactorialCalculator(this, number);
        return factCalc;
    }

    @Override
    public void onLoadFinished(Loader<Integer> loader, Integer factorial) {
        factResult.setText("Result:  " + numForFact.getText().toString()  + "! = "  + factorial);
    }

    @Override
    public void onLoaderReset(Loader<Integer> loader) {
        //nothing to do here
    }

    /*this method gets invoked every time the calculate button is clicked*/
    public void startCalculating(View view) {
        Bundle bundle = new Bundle();
        try {
            bundle.putInt("number", Integer.valueOf(numForFact.getText().toString()));
        }catch (NumberFormatException ex) {
            factResult.setText("");
            Toast wrongInputToast = Toast.makeText(getApplicationContext(),
                    "Enter a numeric value",
                    Toast.LENGTH_SHORT);
            wrongInputToast.setGravity(Gravity.CENTER, 0, 0);
            wrongInputToast.show();
            return;
        }
        getLoaderManager().restartLoader(LOADER_ID, bundle, this).forceLoad();
    }

    private static class FactorialCalculator extends AsyncTaskLoader<Integer> {

        private int number;
        private int factorial;

        public FactorialCalculator(Context context, int number) {
            super(context);
            this.number = number;
        }

        @Override
        public Integer loadInBackground() {
            System.out.println(number);
            factorial = 1;
            for (int i = number; i > 1; i--) {
                factorial *= i;
            }
            return factorial;
        }

    }

}
