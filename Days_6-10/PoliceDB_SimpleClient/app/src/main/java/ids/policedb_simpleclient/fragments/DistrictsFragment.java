package ids.policedb_simpleclient.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import ids.policedb_simpleclient.R;
import ids.policedb_simpleclient.dbutils.DBManager;
import ids.policedb_simpleclient.dbutils.DBOpenHelper;
import ids.policedb_simpleclient.dbutils.DistrictsCursorLoader;

public class DistrictsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    final static int DISTRICTS_LOADER_ID = 1;
    private ListView table;
    private Button add;
    private Button edit;
    private Button delete;
    private Bundle dataToEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data, container, false);

        table = (ListView) view.findViewById(R.id.table);
        table.setSelector(R.drawable.selector_view);

        add = (Button) view.findViewById(R.id.add);
        edit = (Button) view.findViewById(R.id.edit);
        delete = (Button) view.findViewById(R.id.delete);
        TextView heading = (TextView) view.findViewById(R.id.heading);
        heading.setText("Districts");
        getActivity().getSupportLoaderManager().initLoader(DISTRICTS_LOADER_ID, null, this).forceLoad();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddButtonClick();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteButtonClick();
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEditButtonClick();
            }
        });

        table.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor c = (Cursor)adapterView.getItemAtPosition(i);
                dataToEdit = new Bundle();
                dataToEdit.putString(DBOpenHelper.COLUMN_ID, c.getInt(0) + "");
                dataToEdit.putString(DBOpenHelper.COLUMN_DISTRICT_NAME, c.getString(1));
                delete.setEnabled(true);
                edit.setEnabled(true);
            }
        });

        return view;
    }

    private void onAddButtonClick() {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, new NewDistrictRowFragment());
        transaction.addToBackStack("addFragDistrict");
        transaction.commit();
    }

    private void onEditButtonClick() {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        NewDistrictRowFragment fragment = new NewDistrictRowFragment();
        fragment.setArguments(dataToEdit);
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack("addFragDistrict");
        transaction.commit();
    }

    private void onDeleteButtonClick() {
        int districtID = Integer.valueOf(dataToEdit.getString(DBOpenHelper.COLUMN_ID));
        DBManager.getInstance().delete(DBOpenHelper.TABLE_DISTRICTS, districtID);
        getActivity().getSupportLoaderManager().initLoader(DISTRICTS_LOADER_ID, null, this).forceLoad();
        delete.setEnabled(false);
        edit.setEnabled(false);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Context context = getActivity().getApplicationContext();
        return new DistrictsCursorLoader(context, new DBOpenHelper(context));
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        CursorAdapter adapter = new DistrictsCursorAdapter(getActivity().getApplicationContext(), cursor, false);
        table.setAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private class DistrictsCursorAdapter extends CursorAdapter {

        public DistrictsCursorAdapter(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            LayoutInflater inflater = LayoutInflater.from(context);
            return inflater.inflate(R.layout.district_row_view, viewGroup, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView id = (TextView) view.findViewById(R.id.id);
            id.setText(cursor.getInt(cursor.getColumnIndex(DBOpenHelper.COLUMN_ID)) + "");

            TextView districtName = (TextView) view.findViewById(R.id.districtName);
            districtName.setText(cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_DISTRICT_NAME)));
        }
    }

}
