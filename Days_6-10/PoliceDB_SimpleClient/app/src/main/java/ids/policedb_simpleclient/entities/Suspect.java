package ids.policedb_simpleclient.entities;

public class Suspect {

    private int id;
    private String name;
    private String birthDate;
    private String address;
    private String crime;
    private boolean wasConvicted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCrime() {
        return crime;
    }

    public void setCrime(String crime) {
        this.crime = crime;
    }

    public boolean isWasConvicted() {
        return wasConvicted;
    }

    public void setWasConvicted(boolean wasConvicted) {
        this.wasConvicted = wasConvicted;
    }
}
