package ids.policedb_simpleclient.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import ids.policedb_simpleclient.R;
import ids.policedb_simpleclient.dbutils.DBManager;
import ids.policedb_simpleclient.dbutils.DBOpenHelper;
import ids.policedb_simpleclient.dbutils.SuspectDataHolder;

public class NewSuspectRowFragment extends Fragment{

    Button save;
    EditText name;
    EditText birthDate;
    EditText address;
    EditText crime;
    CheckBox conviction;
    Bundle dataToEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_suspect_fragment , container, false);

        save = (Button) view.findViewById(R.id.save);
        name = (EditText) view.findViewById(R.id.name);
        birthDate = (EditText) view.findViewById(R.id.birthDate);
        address = (EditText) view.findViewById(R.id.address);
        crime = (EditText) view.findViewById(R.id.crime);
        conviction = (CheckBox) view.findViewById(R.id.conviction);

        dataToEdit = getArguments();
        if (dataToEdit != null) {
            //edit button is clicked, filling the fields with existing data to edit
            fillWithDataToEdit();
        }

        birthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String START_YEAR = "StartYear";
                final String START_MONTH = "StartMonth";
                final String START_DAY = "StartDay";
                final String END_YEAR = "EndYear";
                final String END_MONTH = "EndMonth";
                final String END_DAY = "EndDay";
                Bundle bundle = new Bundle();
                bundle.putInt(START_YEAR, 1900);
                bundle.putInt(START_MONTH, 1);
                bundle.putInt(START_DAY, 1);
                final Calendar c = Calendar.getInstance();
                bundle.putInt(END_YEAR, c.get(Calendar.YEAR));
                bundle.putInt(END_MONTH, c.get(Calendar.MONTH));
                bundle.putInt(END_DAY, c.get(Calendar.MONTH));
                DateFragment fragment = new DateFragment();
                fragment.setArguments(bundle);
                fragment.setFieldToDisplayDataIn(birthDate);
                fragment.show(getFragmentManager(), "datePicker");
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isEmptyFields()){
                    return;
                }

                SuspectDataHolder dataHolder = new SuspectDataHolder();
                dataHolder.setName(name.getText().toString());
                dataHolder.setBirthDate(birthDate.getText().toString());
                dataHolder.setAddress(address.getText().toString());
                dataHolder.setCrime(crime.getText().toString());
//                dataHolder.setWasConvicted(conviction.getSelectedItem().toString());
                dataHolder.setWasConvicted(conviction.isChecked() );

                name.setText("");
                birthDate.setText("");
                address.setText("");
                crime.setText("");

                if (dataToEdit != null) {
                    dataHolder.setId(dataToEdit.getInt(DBOpenHelper.COLUMN_ID));
                    DBManager.getInstance().updateSuspectRow(dataHolder);
                    Toast.makeText(getActivity().getApplicationContext(), "Table row with id = " + dataHolder.getId() + " was  successfully edited!", Toast.LENGTH_LONG).show();
                } else {
                    DBManager.getInstance().insertIntoSuspects(dataHolder);
                    Toast.makeText(getActivity().getApplicationContext(), "New suspect  " +dataHolder.getName() +" is successfully added!", Toast.LENGTH_LONG).show();
                }

                getFragmentManager().popBackStack("addFragSuspect", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }

            private boolean isEmptyFields() {
                if (name.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter suspect's name", Toast.LENGTH_LONG).show();
                    return true;
                }
                if (birthDate.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter suspect's birth date!", Toast.LENGTH_LONG).show();
                    return true;
                }
                if (address.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter suspect's address!", Toast.LENGTH_LONG).show();
                    return true;
                }
                if (crime.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter suspect's crime!", Toast.LENGTH_LONG).show();
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private void fillWithDataToEdit() {
        name.setText(dataToEdit.getString(DBOpenHelper.COLUMN_NAME));
        birthDate.setText(dataToEdit.getString(DBOpenHelper.COLUMN_BIRTH_DATE));
        address.setText(dataToEdit.getString(DBOpenHelper.COLUMN_ADDRESS));
        crime.setText(dataToEdit.getString(DBOpenHelper.COLUMN_CRIME));
        boolean conv = dataToEdit.getBoolean(DBOpenHelper.COLUMN_WAS_CONVICTED);
        conviction.setChecked(conv);
    }

}
