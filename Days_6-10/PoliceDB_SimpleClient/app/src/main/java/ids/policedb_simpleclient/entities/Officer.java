package ids.policedb_simpleclient.entities;

public class Officer {

    private int id;
    private String name;
    private String birthDate;
    private String address;
    private int numOfArrests;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumOfArrests() {
        return numOfArrests;
    }

    public void setNumOfArrests(int numOfArrests) {
        this.numOfArrests = numOfArrests;
    }
}
