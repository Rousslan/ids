package ids.policedb_simpleclient.adapters;

import android.content.Context;
import android.database.Cursor;

import ids.policedb_simpleclient.StartActivity;
import ids.policedb_simpleclient.dbutils.DBOpenHelper;
import ids.policedb_simpleclient.dbutils.SuspectsCursorLoader;

public class SuspectFieldAdapter extends BaseFieldAdapter {

    public SuspectFieldAdapter(Context context) {
        super(context);
    }

    @Override
    protected void loadDataIntoMap() {
        Cursor cursor = new SuspectsCursorLoader(getContext(), StartActivity.dbOpenHelper).loadInBackground();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(cursor.getColumnIndex(DBOpenHelper.COLUMN_ID));
            String name = cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_NAME));
            iDsAndNames.put(id, name);
            cursor.moveToNext();
        }
    }

}
