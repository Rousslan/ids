package ids.policedb_simpleclient.dbutils;

import android.content.Context;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.CursorLoader;

public abstract class BaseCursorLoader extends CursorLoader {

    private SQLiteDatabase database;

    protected BaseCursorLoader(Context context, DBOpenHelper dbOpenHelper) {
        super(context);
        this.database = dbOpenHelper.getReadableDatabase();
    }

    protected SQLiteDatabase getDatabase() {
        return database;
    }

    @Override
    public abstract Cursor loadInBackground();
}
