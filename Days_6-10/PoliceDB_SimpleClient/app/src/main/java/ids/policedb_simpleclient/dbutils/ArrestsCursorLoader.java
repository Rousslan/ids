package ids.policedb_simpleclient.dbutils;

import android.content.Context;
import android.database.Cursor;

public  class ArrestsCursorLoader extends BaseCursorLoader{

    public ArrestsCursorLoader(Context context, DBOpenHelper dbOpenHelper) {
        super(context, dbOpenHelper);
    }

    @Override
    public Cursor loadInBackground() {
        String[] columns = new String[] { DBOpenHelper.COLUMN_ID,
                                                                                  DBOpenHelper.COLUMN_OFFICER_ID,
                                                                                  DBOpenHelper.COLUMN_SUSPECT_ID,
                                                                                  DBOpenHelper.COLUMN_ARREST_DATE,
                                                                                  DBOpenHelper.COLUMN_DISTRICT_ID,
                                                                                  DBOpenHelper.COLUMN_CRIME};
        return  getDatabase().query(DBOpenHelper.TABLE_ARRESTS, columns, null, null, null, null, null);
    }
}
