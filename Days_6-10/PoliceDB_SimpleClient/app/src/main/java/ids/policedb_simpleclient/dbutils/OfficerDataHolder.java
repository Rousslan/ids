package ids.policedb_simpleclient.dbutils;

import android.content.ContentValues;

public class OfficerDataHolder {

    private int id;
    private String name;
    private String birthDate;
    private String address;

    public OfficerDataHolder() { }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DBOpenHelper.COLUMN_NAME,  name);
        cv.put(DBOpenHelper.COLUMN_BIRTH_DATE,  birthDate);
        cv.put(DBOpenHelper.COLUMN_ADDRESS,  address);
        return cv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}