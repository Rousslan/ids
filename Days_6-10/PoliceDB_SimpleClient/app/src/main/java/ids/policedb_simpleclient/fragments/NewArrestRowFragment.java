package ids.policedb_simpleclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import ids.policedb_simpleclient.R;
import ids.policedb_simpleclient.adapters.DistrictFieldAdapter;
import ids.policedb_simpleclient.adapters.OfficerFieldAdapter;
import ids.policedb_simpleclient.adapters.SuspectFieldAdapter;
import ids.policedb_simpleclient.dbutils.ArrestDataHolder;
import ids.policedb_simpleclient.dbutils.DBManager;
import ids.policedb_simpleclient.dbutils.DBOpenHelper;

public class NewArrestRowFragment extends Fragment {

    private Button save;
    private Spinner officer;
    private Spinner suspect;
    private EditText arrestDate;
    private Spinner district;
    private EditText crime;
    private Bundle dataToEdit;
    private DistrictFieldAdapter districtView;
    private OfficerFieldAdapter officerView;
    private SuspectFieldAdapter  suspectView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.new_arrest_fragment, container, false);

        save = (Button) view.findViewById(R.id.save);
        officer = (Spinner) view.findViewById(R.id.officer);
        suspect = (Spinner) view.findViewById(R.id.suspect);
        arrestDate = (EditText) view.findViewById(R.id.arrestDate);
        district = (Spinner) view.findViewById(R.id.district);
        crime = (EditText) view.findViewById(R.id.crime);

        //fetch data about existing officers, suspects and districtNames from DB
        //and display names, substituting IDs with the help of custom adapters
        Context context= getActivity().getApplicationContext();
        officerView = new OfficerFieldAdapter(context);
        List<String> officersNames = officerView.getNames();
        districtView = new DistrictFieldAdapter(context);
        List<String> districtNames = districtView.getNames();
        suspectView = new SuspectFieldAdapter(context);
        List<String> suspectsNames = suspectView.getNames();

        ArrayAdapter<String> officersAdapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item , officersNames);
        officer.setAdapter(officersAdapter);
        ArrayAdapter<String> suspectAdapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item , suspectsNames);
        suspect.setAdapter(suspectAdapter);
        ArrayAdapter<String > districtsAdapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item , districtNames);
        district.setAdapter(districtsAdapter);

        dataToEdit = getArguments();
        if (dataToEdit != null) {
            //edit button is clicked, filling the fields with existing data to edit
            fillWithDataToEdit();
        }

        arrestDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String START_YEAR = "StartYear";
                final String START_MONTH = "StartMonth";
                final String START_DAY = "StartDay";
                final String END_YEAR = "EndYear";
                final String END_MONTH = "EndMonth";
                final String END_DAY = "EndDay";
                Bundle bundle = new Bundle();
                bundle.putInt(START_YEAR, 2010);
                bundle.putInt(START_MONTH, 1);
                bundle.putInt(START_DAY, 1);
                final Calendar c = Calendar.getInstance();
                bundle.putInt(END_YEAR, c.get(Calendar.YEAR));
                bundle.putInt(END_MONTH, c.get(Calendar.MONTH));
                bundle.putInt(END_DAY, c.get(Calendar.MONTH));
                DateFragment fragment = new DateFragment();
                fragment.setArguments(bundle);
                fragment.setFieldToDisplayDataIn(arrestDate);
                fragment.show(getFragmentManager(), "datePicker");
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEmptyFields()){
                    return;
                }

                ArrestDataHolder dataHolder = new ArrestDataHolder();
                dataHolder.setOfficerID(officerView.getIDByName(officer.getSelectedItem().toString()));
                dataHolder.setSuspectID(suspectView.getIDByName(suspect.getSelectedItem().toString()));
                dataHolder.setArrestDate(arrestDate.getText().toString());
                dataHolder.setDistrictID(districtView.getIDByName(district.getSelectedItem().toString()));
                dataHolder.setCrime(crime.getText().toString());

                arrestDate.setText("");
                crime.setText("");

                if (dataToEdit != null) {
//                    data[5] = dataToEdit.getInt(DBOpenHelper.COLUMN_ID) + "";
                    dataHolder.setId(dataToEdit.getInt(DBOpenHelper.COLUMN_ID) );
                    DBManager.getInstance().updateArrestRow(dataHolder);
                    Toast.makeText(getActivity().getApplicationContext(), "Table row with id = " + dataHolder.getId()+ " was  successfully edited!", Toast.LENGTH_LONG).show();
                } else {
                    DBManager.getInstance().insertIntoArrests(dataHolder);
                    Toast.makeText(getActivity().getApplicationContext(), "New arrest  is successfully added!", Toast.LENGTH_LONG).show();
                }
                getFragmentManager().popBackStack("addFragArrest", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }

            private boolean isEmptyFields() {
                if (arrestDate.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter arrest date!", Toast.LENGTH_LONG).show();
                    return true;
                }
                if (crime.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please the crime!", Toast.LENGTH_LONG).show();
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private void fillWithDataToEdit() {
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) officer.getAdapter();
        int i = adapter.getPosition(dataToEdit.getString(DBOpenHelper.COLUMN_OFFICER_ID));
        officer.setSelection(i);
        adapter = (ArrayAdapter<String>) suspect.getAdapter();
        i = adapter.getPosition(dataToEdit.getString(DBOpenHelper.COLUMN_SUSPECT_ID));
        suspect.setSelection(i);
        arrestDate.setText(dataToEdit.getString(DBOpenHelper.COLUMN_ARREST_DATE));
        adapter = (ArrayAdapter<String>) district.getAdapter();
        i = adapter.getPosition(dataToEdit.getString(DBOpenHelper.COLUMN_DISTRICT_ID));
        district.setSelection(i);
        crime.setText(dataToEdit.getString(DBOpenHelper.COLUMN_CRIME));
    }

}
