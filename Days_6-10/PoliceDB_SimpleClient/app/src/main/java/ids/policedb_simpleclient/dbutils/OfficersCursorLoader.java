package ids.policedb_simpleclient.dbutils;

import android.content.Context;
import android.database.Cursor;

public  class OfficersCursorLoader extends BaseCursorLoader{

    public OfficersCursorLoader(Context context, DBOpenHelper dbOpenHelper) {
        super(context, dbOpenHelper);
    }

    @Override
    public Cursor loadInBackground() {
        String[] columns = new String[] {DBOpenHelper.COLUMN_ID,
                                                                                DBOpenHelper.COLUMN_NAME ,
                                                                                DBOpenHelper.COLUMN_BIRTH_DATE ,
                                                                                DBOpenHelper.COLUMN_ADDRESS ,
                                                                                DBOpenHelper.COLUMN_NUMBER_OF_ARRESTS };
        return  getDatabase().query(DBOpenHelper.TABLE_OFFICERS, columns, null, null, null, null, null);
    }
}
