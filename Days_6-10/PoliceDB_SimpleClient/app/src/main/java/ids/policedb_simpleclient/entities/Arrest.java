package ids.policedb_simpleclient.entities;

public class Arrest {

    private int id;
    private Officer officer;
    private Suspect suspect;
    private String arrestDate;
    private District arrestDistrict;
    private String crime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Officer getOfficer() {
        return officer;
    }

    public void setOfficer(Officer officer) {
        this.officer = officer;
    }

    public Suspect getSuspect() {
        return suspect;
    }

    public void setSuspect(Suspect suspect) {
        this.suspect = suspect;
    }

    public String getArrestDate() {
        return arrestDate;
    }

    public void setArrestDate(String arrestDate) {
        this.arrestDate = arrestDate;
    }

    public District getArrestDistrict() {
        return arrestDistrict;
    }

    public void setArrestDistrict(District arrestDistrict) {
        this.arrestDistrict = arrestDistrict;
    }

    public String getCrime() {
        return crime;
    }

    public void setCrime(String crime) {
        this.crime = crime;
    }
}
