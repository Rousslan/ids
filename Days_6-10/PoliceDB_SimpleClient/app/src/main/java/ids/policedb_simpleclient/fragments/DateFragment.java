package ids.policedb_simpleclient.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class DateFragment extends DialogFragment
                                                               implements DatePickerDialog.OnDateSetListener {

    final String START_YEAR = "StartYear";
    final String START_MONTH = "StartMonth";
    final String START_DAY = "StartDay";
    final String END_YEAR = "EndYear";
    final String END_MONTH = "EndMonth";
    final String END_DAY = "EndDay";
    private int year;
    private int month;
    private int day;
    private EditText dateField;

    public DateFragment() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        this.year = bundle.getInt(START_YEAR);
        this.month = bundle.getInt(START_MONTH);
        this.day = bundle.getInt(START_DAY);
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
        Bundle bundle = getArguments();
        int endYear = bundle.getInt(END_YEAR);
        int endMonth = bundle.getInt(END_MONTH);
        int endDay = bundle.getInt(END_DAY);
        boolean wrongYear = datePicker.getYear() > endYear;
        boolean wrongMonth = datePicker.getMonth() > endMonth;
        boolean wrongDay = datePicker.getDayOfMonth() > endDay;
        if (wrongYear || wrongMonth || wrongDay) {
            Toast.makeText(getActivity().getApplicationContext(), "Setting future date is not appropriate!", Toast.LENGTH_LONG).show();
            return;
        }
        dateField.setText(datePicker.getDayOfMonth() + " / " + datePicker.getMonth() + " / " + datePicker.getYear());
    }

    public void setFieldToDisplayDataIn(EditText dateField) {
        this.dateField = dateField;
    }


}
