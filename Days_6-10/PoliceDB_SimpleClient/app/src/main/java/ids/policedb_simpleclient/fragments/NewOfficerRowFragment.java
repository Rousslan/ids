package ids.policedb_simpleclient.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import ids.policedb_simpleclient.R;
import ids.policedb_simpleclient.dbutils.DBManager;
import ids.policedb_simpleclient.dbutils.DBOpenHelper;
import ids.policedb_simpleclient.dbutils.OfficerDataHolder;

public class NewOfficerRowFragment extends Fragment {

    Button save;
    EditText name;
    EditText birthDate;
    EditText address;
    Bundle dataToEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_officer_fragment , container, false);

        save = (Button) view.findViewById(R.id.save);
        name = (EditText) view.findViewById(R.id.name);
        birthDate = (EditText) view.findViewById(R.id.birthDate);
        address = (EditText) view.findViewById(R.id.address);
        dataToEdit = getArguments();
        if (dataToEdit != null) {
            //edit button is clicked, filling the fields with existing data to edit
            fillWithDataToEdit();
        }

        birthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String START_YEAR = "StartYear";
                final String START_MONTH = "StartMonth";
                final String START_DAY = "StartDay";
                final String END_YEAR = "EndYear";
                final String END_MONTH = "EndMonth";
                final String END_DAY = "EndDay";
                Bundle bundle = new Bundle();
                bundle.putInt(START_YEAR, 1900);
                bundle.putInt(START_MONTH, 1);
                bundle.putInt(START_DAY, 1);
                final Calendar c = Calendar.getInstance();
                bundle.putInt(END_YEAR, c.get(Calendar.YEAR));
                bundle.putInt(END_MONTH, c.get(Calendar.MONTH));
                bundle.putInt(END_DAY, c.get(Calendar.MONTH));
                DateFragment fragment = new DateFragment();
                fragment.setArguments(bundle);
                fragment.setFieldToDisplayDataIn(birthDate);
                fragment.show(getFragmentManager(), "datePicker");
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isEmptyFields()){
                    return;
                }

                OfficerDataHolder dataHolder = new OfficerDataHolder();
                dataHolder.setName(name.getText().toString());
                dataHolder.setAddress(address.getText().toString());
                dataHolder.setBirthDate(birthDate.getText().toString());

                name.setText("");
                birthDate.setText("");
                address.setText("");

                if (dataToEdit != null) {
                    dataHolder.setId(dataToEdit.getInt(DBOpenHelper.COLUMN_ID));
                    DBManager.getInstance().updateOfficerRow(dataHolder);
                    Toast.makeText(getActivity().getApplicationContext(), "Table row with id = " +dataHolder.getId()+" was  successfully edited!", Toast.LENGTH_LONG).show();
                } else {
                    DBManager.getInstance().insertIntoOfficers(dataHolder);
                    Toast.makeText(getActivity().getApplicationContext(), "New officer  " +dataHolder.getName() +" is successfully added!", Toast.LENGTH_LONG).show();
                }
                getFragmentManager().popBackStack("addFragOfficer", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }

            private boolean isEmptyFields() {
                if (name.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter officer's name", Toast.LENGTH_LONG).show();
                    return true;
                }
                if (birthDate.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter officer's birth date!", Toast.LENGTH_LONG).show();
                    return true;
                }
                if (address.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter officer's address!", Toast.LENGTH_LONG).show();
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private void fillWithDataToEdit() {
        name.setText(dataToEdit.getString(DBOpenHelper.COLUMN_NAME));
        birthDate.setText(dataToEdit.getString(DBOpenHelper.COLUMN_BIRTH_DATE));
        address.setText(dataToEdit.getString(DBOpenHelper.COLUMN_ADDRESS));
    }

}
