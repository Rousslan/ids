package ids.policedb_simpleclient.dbutils;

import android.content.ContentValues;

public class SuspectDataHolder {

    private int id;
    private String name;
    private String birthDate;
    private String address;
    private String crime;
    private boolean wasConvicted;

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DBOpenHelper.COLUMN_NAME,  name);
        cv.put(DBOpenHelper.COLUMN_BIRTH_DATE, birthDate);
        cv.put(DBOpenHelper.COLUMN_ADDRESS,  address);
        cv.put(DBOpenHelper.COLUMN_CRIME , crime);
        cv.put(DBOpenHelper.COLUMN_WAS_CONVICTED , wasConvicted);

        return cv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCrime() {
        return crime;
    }

    public void setCrime(String crime) {
        this.crime = crime;
    }

    public boolean isWasConvicted() {
        return wasConvicted;
    }

    public void setWasConvicted(Boolean wasConvicted) {
        this.wasConvicted = wasConvicted;

//        this.wasConvicted = (wasConvicted.equals("Yes"));
    }
}
