package ids.policedb_simpleclient.entities;

public class District {

    private int id;
    private String districtName;

    public District() {
        districtName = "";
    }

    public District(int id, String districtName) {
        this.id = id;
        this.districtName = districtName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    /*This overriding is temporary in the aims of debugging*/
    @Override
    public String toString() {
        return "District{" +
                "id=" + id +
                ", districtName='" + districtName + '\'' +
                '}';
    }
}
