package ids.policedb_simpleclient.fragments;


import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import ids.policedb_simpleclient.R;
import ids.policedb_simpleclient.dbutils.DBManager;
import ids.policedb_simpleclient.dbutils.DBOpenHelper;
import ids.policedb_simpleclient.dbutils.SuspectsCursorLoader;

public class SuspectsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private final static int SUSPECTS_LOADER_ID = 3;
    private ListView table;
    private Button add;
    private Button edit;
    private Button delete;
    //Bundle just for case if existing data needs to be edited.
    //This bundle filled with existing data is to be passed to a new fragment.
    Bundle dataToEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data, container, false);

        table = (ListView) view.findViewById(R.id.table);
        table.setSelector(R.drawable.selector_view);

        add = (Button) view.findViewById(R.id.add);
        edit = (Button) view.findViewById(R.id.edit);
        delete = (Button) view.findViewById(R.id.delete);
        TextView heading = (TextView) view.findViewById(R.id.heading);
        heading.setText("Suspects");

        getActivity().getSupportLoaderManager().initLoader(SUSPECTS_LOADER_ID, null, this).forceLoad();
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddButtonClick();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteButtonClick();
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEditButtonClick();
            }
        });

        table.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor c = (Cursor)adapterView.getItemAtPosition(i);
                //filling the bundle for the possible editing after list's item is selected
                dataToEdit = new Bundle();
                dataToEdit.putInt(DBOpenHelper.COLUMN_ID, c.getInt(c.getColumnIndex(DBOpenHelper.COLUMN_ID)));
                dataToEdit.putString(DBOpenHelper.COLUMN_NAME, c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_NAME)));
                dataToEdit.putString(DBOpenHelper.COLUMN_BIRTH_DATE, c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_BIRTH_DATE)));
                dataToEdit.putString(DBOpenHelper.COLUMN_ADDRESS, c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_ADDRESS)));
                dataToEdit.putString(DBOpenHelper.COLUMN_CRIME, c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_CRIME)));
//                dataToEdit.putString(DBOpenHelper.COLUMN_WAS_CONVICTED, c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_WAS_CONVICTED)));
                boolean conv =( c.getInt(c.getColumnIndex(DBOpenHelper.COLUMN_WAS_CONVICTED)) == 1);
                dataToEdit.putBoolean(DBOpenHelper.COLUMN_WAS_CONVICTED,  conv);
                delete.setEnabled(true);
                edit.setEnabled(true);
            }
        });
        return view;
    }

    private void onEditButtonClick() {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        NewSuspectRowFragment fragment = new NewSuspectRowFragment();
        fragment.setArguments(dataToEdit);
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack("addFragSuspect");
        transaction.commit();
    }

    private void onDeleteButtonClick() {
        int suspectID = dataToEdit.getInt(DBOpenHelper.COLUMN_ID);
        DBManager.getInstance().delete(DBOpenHelper.TABLE_SUSPECTS, suspectID);
        getActivity().getSupportLoaderManager().restartLoader(SUSPECTS_LOADER_ID, null, this).forceLoad();
        delete.setEnabled(false);
        edit.setEnabled(false);
    }

    public void onAddButtonClick() {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, new NewSuspectRowFragment());
        transaction.addToBackStack("addFragSuspect");
        transaction.commit();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Context context = getActivity().getApplicationContext();
        return new SuspectsCursorLoader(context, new DBOpenHelper(context));
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        CursorAdapter adapter = new SuspectsCursorAdapter(getActivity().getApplicationContext(), cursor, false);
        table.setAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private class SuspectsCursorAdapter extends CursorAdapter {

        public SuspectsCursorAdapter(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            LayoutInflater inflater = LayoutInflater.from(context);
            return inflater.inflate(R.layout.suspects_row_view, viewGroup, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView id = (TextView) view.findViewById(R.id.id);
            id.setText(cursor.getInt(cursor.getColumnIndex(DBOpenHelper.COLUMN_ID)) + "");
            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText(cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_NAME)));
            TextView birthDate= (TextView) view.findViewById(R.id.birthDate);
            birthDate.setText(cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_BIRTH_DATE)));
            TextView address = (TextView) view.findViewById(R.id.address);
            address.setText(cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_ADDRESS)));
            TextView crime = (TextView) view.findViewById(R.id.crime);
            crime.setText(cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_CRIME)));
            TextView conviction = (TextView) view.findViewById(R.id.conviction);
            int conv = cursor.getInt(cursor.getColumnIndex(DBOpenHelper.COLUMN_WAS_CONVICTED));
            conviction.setText( (conv == 1) ? "Yes" : "No");
        }
    }

}
