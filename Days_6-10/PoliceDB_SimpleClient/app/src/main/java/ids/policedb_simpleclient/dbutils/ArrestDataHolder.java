package ids.policedb_simpleclient.dbutils;

import android.content.ContentValues;

public class ArrestDataHolder {

    private int id;
    private int officerID;
    private int suspectID;
    private String arrestDate;
    private int districtID;
    private String crime;

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DBOpenHelper.COLUMN_OFFICER_ID, officerID);
        cv.put(DBOpenHelper.COLUMN_SUSPECT_ID, suspectID);
        cv.put(DBOpenHelper.COLUMN_ARREST_DATE, arrestDate);
        cv.put(DBOpenHelper.COLUMN_DISTRICT_ID, districtID);
        cv.put(DBOpenHelper.COLUMN_CRIME, crime);
        return cv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOfficerID() {
        return officerID;
    }

    public void setOfficerID(int officerID) {
        this.officerID = officerID;
    }

    public int getSuspectID() {
        return suspectID;
    }

    public void setSuspectID(int suspectID) {
        this.suspectID = suspectID;
    }

    public String getArrestDate() {
        return arrestDate;
    }

    public void setArrestDate(String arrestDate) {
        this.arrestDate = arrestDate;
    }

    public int getDistrictID() {
        return districtID;
    }

    public void setDistrictID(int districtID) {
        this.districtID = districtID;
    }

    public String getCrime() {
        return crime;
    }

    public void setCrime(String crime) {
        this.crime = crime;
    }
}
