package ids.policedb_simpleclient.fragments;

import android.support.v4.app.Fragment;;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ids.policedb_simpleclient.R;

public class StartFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                                                  Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_start, container, false);
    }

}
