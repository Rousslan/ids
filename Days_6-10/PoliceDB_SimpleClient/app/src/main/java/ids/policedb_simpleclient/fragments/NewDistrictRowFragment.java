package ids.policedb_simpleclient.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ids.policedb_simpleclient.R;
import ids.policedb_simpleclient.dbutils.DBManager;
import ids.policedb_simpleclient.dbutils.DBOpenHelper;

public class NewDistrictRowFragment extends Fragment {

    Button save;
    EditText newDistrict;
    Bundle dataToEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_district_fragment, container, false);

        save = (Button) view.findViewById(R.id.saveNewDistrict);
        newDistrict = (EditText) view.findViewById(R.id.newDistrict);

        dataToEdit = getArguments();
        if (dataToEdit != null) {
            //edit button is clicked, filling the fields with existing data to edit
           newDistrict.setText(dataToEdit.getString(DBOpenHelper.COLUMN_DISTRICT_NAME));
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (newDistrict.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter new district's name", Toast.LENGTH_LONG).show();
                    return;
                }

                String[] data = new String[2];
                data[1] = newDistrict.getText().toString();

                newDistrict.setText("");
                if (dataToEdit != null) {
                    data[0] = dataToEdit.getString(DBOpenHelper.COLUMN_ID);
                    DBManager.getInstance().updateDistrictRow(data);
                    Toast.makeText(getActivity().getApplicationContext(), "The row with id = " + data[0] +" was successfully edited!", Toast.LENGTH_LONG).show();
                } else {
                    DBManager.getInstance().insertIntoDistricts(data[1]);
                    Toast.makeText(getActivity().getApplicationContext(), "New row with district " + data[1] +" is successfully added!", Toast.LENGTH_LONG).show();
                }
                getFragmentManager().popBackStack("addFragDistrict", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
        return view;
    }

}
