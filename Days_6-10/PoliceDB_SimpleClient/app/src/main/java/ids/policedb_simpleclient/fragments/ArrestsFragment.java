package ids.policedb_simpleclient.fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import ids.policedb_simpleclient.R;
import ids.policedb_simpleclient.adapters.DistrictFieldAdapter;
import ids.policedb_simpleclient.adapters.OfficerFieldAdapter;
import ids.policedb_simpleclient.adapters.SuspectFieldAdapter;
import ids.policedb_simpleclient.dbutils.ArrestsCursorLoader;
import ids.policedb_simpleclient.dbutils.DBManager;
import ids.policedb_simpleclient.dbutils.DBOpenHelper;

public class ArrestsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    final static int ARRESTS_LOADER_ID = 4;
    private ListView table;
    private Button add;
    private Button edit;
    private Button delete;
    //Bundle just for case if existing data needs to be edited.
    //This bundle filled with existing data is to be passed to a new fragment.
    Bundle dataToEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data, container, false);

        table = (ListView) view.findViewById(R.id.table);
        table.setSelector(R.drawable.selector_view);

        add = (Button) view.findViewById(R.id.add);
        edit = (Button) view.findViewById(R.id.edit);
        delete = (Button) view.findViewById(R.id.delete);
        TextView heading = (TextView) view.findViewById(R.id.heading);
        heading.setText("Arrests");

         getActivity().getSupportLoaderManager().initLoader(ARRESTS_LOADER_ID, null, this).forceLoad();
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddButtonClick();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteButtonClick();
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEditButtonClick();
            }
        });

        table.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor c = (Cursor)adapterView.getItemAtPosition(i);
                //filling the bundle for the possible editing after list's item is selected
                dataToEdit = new Bundle();
                dataToEdit.putInt(DBOpenHelper.COLUMN_ID, c.getInt(c.getColumnIndex(DBOpenHelper.COLUMN_ID)) );
                String officerByID = new OfficerFieldAdapter(getActivity().getApplicationContext())
                                                                       .getNameByID(c.getInt(c.getColumnIndex(DBOpenHelper.COLUMN_OFFICER_ID)));
                dataToEdit.putString(DBOpenHelper.COLUMN_OFFICER_ID, officerByID);
                String districtByID = new DistrictFieldAdapter(getActivity().getApplicationContext())
                                                                        .getNameByID(c.getInt(c.getColumnIndex(DBOpenHelper.COLUMN_DISTRICT_ID)));
                dataToEdit.putString(DBOpenHelper.COLUMN_DISTRICT_ID, districtByID);
                String suspectByID = new SuspectFieldAdapter(getActivity().getApplicationContext())
                                                                          .getNameByID(c.getInt(c.getColumnIndex(DBOpenHelper.COLUMN_SUSPECT_ID)));
                dataToEdit.putString(DBOpenHelper.COLUMN_SUSPECT_ID, suspectByID);
                dataToEdit.putString(DBOpenHelper.COLUMN_ARREST_DATE, c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_ARREST_DATE)));
                dataToEdit.putString(DBOpenHelper.COLUMN_CRIME, c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_CRIME)));
                delete.setEnabled(true);
                edit.setEnabled(true);
            }
        });
        return view;
    }

    private void onEditButtonClick() {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        NewArrestRowFragment fragment = new NewArrestRowFragment();
        fragment.setArguments(dataToEdit);
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack("addFragArrest");
        transaction.commit();
    }

    private void onDeleteButtonClick() {
        int arrestID = dataToEdit.getInt(DBOpenHelper.COLUMN_ID);
        DBManager.getInstance().delete(DBOpenHelper.TABLE_ARRESTS, arrestID);
        getActivity().getSupportLoaderManager().restartLoader(ARRESTS_LOADER_ID, null, this).forceLoad();
        delete.setEnabled(false);
        edit.setEnabled(false);
    }

    public void onAddButtonClick() {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, new NewArrestRowFragment());
        transaction.addToBackStack("addFragArrest");
        transaction.commit();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Context context = getActivity().getApplicationContext();
        return new ArrestsCursorLoader(context, new DBOpenHelper(context));
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        CursorAdapter adapter = new ArrestsCursorAdapter(getActivity().getApplicationContext(), cursor, false);
        table.setAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }


    private class ArrestsCursorAdapter extends CursorAdapter {

        public ArrestsCursorAdapter(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            LayoutInflater inflater = LayoutInflater.from(context);
            return inflater.inflate(R.layout.arrests_row_view, viewGroup, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView id = (TextView) view.findViewById(R.id.id);
            id.setText(cursor.getInt(cursor.getColumnIndex(DBOpenHelper.COLUMN_ID)) + "");

            TextView officer = (TextView) view.findViewById(R.id.officerID);
            String o = new OfficerFieldAdapter(context).getNameByID(cursor.getInt(cursor.getColumnIndex(DBOpenHelper.COLUMN_OFFICER_ID)));
            officer.setText(o);

            TextView suspect = (TextView) view.findViewById(R.id.suspectID);
            String s = new SuspectFieldAdapter(context).getNameByID((cursor.getInt(cursor.getColumnIndex(DBOpenHelper.COLUMN_SUSPECT_ID))));
            suspect.setText(s);

            TextView arrestDate = (TextView) view.findViewById(R.id.arrestDate);
            arrestDate.setText(cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_ARREST_DATE)) );

            TextView district = (TextView) view.findViewById(R.id.districtID);
            String d = new DistrictFieldAdapter (context).getNameByID(cursor.getInt(cursor.getColumnIndex(DBOpenHelper.COLUMN_DISTRICT_ID)));
            district.setText(d);

            TextView crime = (TextView) view.findViewById(R.id.crime);
            crime.setText(cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_CRIME)));
        }
    }
}
