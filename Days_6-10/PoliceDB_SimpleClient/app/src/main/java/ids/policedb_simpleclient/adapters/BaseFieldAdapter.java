package ids.policedb_simpleclient.adapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class BaseFieldAdapter {

    private Context context;
    protected List<String> names;
    protected Map<Integer, String> iDsAndNames;

    protected BaseFieldAdapter(Context context) {
        this.context = context;
        iDsAndNames = new HashMap<>();
        loadDataIntoMap();
        names = new ArrayList<>(iDsAndNames.values());
    }

    protected abstract void loadDataIntoMap();

    protected Context getContext() {
        return context;
    }

    public List<String> getNames() {
        return names;
    }

    public int getIDByName(String Name) {
        int id = -1;
        Set<Map.Entry<Integer, String>> entrySet = iDsAndNames.entrySet();
        for (Map.Entry<Integer, String> entry : entrySet) {
            if (Name.equals(entry.getValue())) {
                id = entry.getKey();
            }
        }
        //Possible problem: if several rows with different IDs have same district names
        return id;
    }

    public String getNameByID(int id) {
        return iDsAndNames.get(id);
    }

}
