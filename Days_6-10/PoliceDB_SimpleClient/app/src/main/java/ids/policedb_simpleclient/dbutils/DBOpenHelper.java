package ids.policedb_simpleclient.dbutils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DBOpenHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "policeDB";
    // Table names
    public static final String TABLE_DISTRICTS= "districts";
    public static final String TABLE_SUSPECTS= "suspects";
    public static final String TABLE_OFFICERS= "officers";
    public static final String TABLE_ARRESTS= "arrests";
    // id constant for all the tables
    public static final String COLUMN_ID = "_id";
    // Districts' table's column names
    public static final String COLUMN_DISTRICT_NAME= "DistrictName";
    // Suspects' table's column names
    public static final String COLUMN_NAME = "Name";
    public static final String COLUMN_BIRTH_DATE = "Birthdate";
    public static final String COLUMN_CRIME = "Crime";
    public static final String COLUMN_ADDRESS = "Address";
    public static final String COLUMN_WAS_CONVICTED = "WasConvicted";
    // Officers' table's column names
    public static final String COLUMN_NUMBER_OF_ARRESTS = "ArrestsNumber";
    // Arrests' table's column names
    public static final String COLUMN_OFFICER_ID = "OfficerID";
    public static final String COLUMN_SUSPECT_ID =  "SuspectID";
    public static final String COLUMN_ARREST_DATE = "ArrestDate";
    public static final String COLUMN_DISTRICT_ID = "DistrictID";

    private Context context;

    public  DBOpenHelper(Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
        this.context = context;
    }

    /*this database schema might be not final , slight alterations are possible*/
    @Override
    public void onCreate(SQLiteDatabase database) {
        String CREATE_DISTRICTS_TABLE = "CREATE TABLE " + TABLE_DISTRICTS +
                                                                                  "( " + COLUMN_ID + " INTEGER PRIMARY KEY,  "
                                                                                          + COLUMN_DISTRICT_NAME + " TEXT " + " )";
        database.execSQL(CREATE_DISTRICTS_TABLE);
        Toast.makeText(context, "DISTRICTS created!", Toast.LENGTH_SHORT).show();

        String CREATE_OFFICERS_TABLE = "CREATE TABLE " + TABLE_OFFICERS +
                                                                                "(" + COLUMN_ID + " INTEGER PRIMARY KEY, "
                                                                                       + COLUMN_NAME + " TEXT, "
                                                                                       + COLUMN_BIRTH_DATE + " TEXT, "
                                                                                       + COLUMN_ADDRESS + " TEXT, "
                                                                                       + COLUMN_NUMBER_OF_ARRESTS + " TEXT " + ")";
        database.execSQL(CREATE_OFFICERS_TABLE);
        Toast.makeText(context, "OFFICERS created!", Toast.LENGTH_SHORT).show();

        String CREATE_SUSPECTS_TABLE = "CREATE TABLE " + TABLE_SUSPECTS +
                                                                                  "( " + COLUMN_ID + " INTEGER PRIMARY KEY, "
                                                                                          + COLUMN_NAME + " TEXT, "
                                                                                          + COLUMN_BIRTH_DATE + " TEXT, "
                                                                                          + COLUMN_ADDRESS + " TEXT, "
                                                                                          + COLUMN_CRIME + " TEXT, "
                                                                                          + COLUMN_WAS_CONVICTED + " INTEGER " + " )";
        database.execSQL(CREATE_SUSPECTS_TABLE);
        Toast.makeText(context, "SUSPECTS created!", Toast.LENGTH_SHORT).show();

        String CREATE_ARRESTS_TABLE = "CREATE TABLE " + TABLE_ARRESTS  +
                                                                                "( " + COLUMN_ID + " INTEGER PRIMARY KEY, "
                                                                                        + COLUMN_OFFICER_ID + " INTEGER, "
                                                                                        + COLUMN_SUSPECT_ID + " INTEGER, "
                                                                                        + COLUMN_ARREST_DATE + " TEXT, "
                                                                                        + COLUMN_DISTRICT_ID+ " INTEGER, "
                                                                                        + COLUMN_CRIME + " TEXT, "
                                                                                        + "FOREIGN KEY (" + COLUMN_OFFICER_ID + ") REFERENCES "
                                                                                        + TABLE_OFFICERS + "(" + COLUMN_ID + ") ON DELETE CASCADE "
                                                                                        + "FOREIGN KEY (" + COLUMN_DISTRICT_ID + ") REFERENCES "
                                                                                        + TABLE_DISTRICTS + "(" + COLUMN_ID + ") ON DELETE CASCADE "
                                                                                        + "FOREIGN KEY (" + COLUMN_SUSPECT_ID + ") REFERENCES "
                                                                                        + TABLE_SUSPECTS+ "(" + COLUMN_ID + ") ON DELETE CASCADE"+ " )";
        database.execSQL(CREATE_ARRESTS_TABLE);
        Toast.makeText(context, "ARRESTS created!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i2) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_DISTRICTS);
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFICERS);
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_SUSPECTS);
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_ARRESTS);
        onCreate(database);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }
}

