package ids.policedb_simpleclient.dbutils;

import android.content.Context;
import android.database.Cursor;

public  class SuspectsCursorLoader extends BaseCursorLoader{

    public SuspectsCursorLoader(Context context, DBOpenHelper dbOpenHelper) {
        super(context, dbOpenHelper);
    }

    @Override
    public Cursor loadInBackground() {
        String[] columns = new String[] {DBOpenHelper.COLUMN_ID,
                                                                                 DBOpenHelper.COLUMN_NAME ,
                                                                                 DBOpenHelper.COLUMN_BIRTH_DATE ,
                                                                                 DBOpenHelper.COLUMN_ADDRESS ,
                                                                                 DBOpenHelper.COLUMN_CRIME,
                                                                                 DBOpenHelper.COLUMN_WAS_CONVICTED};
        return getDatabase().query(DBOpenHelper.TABLE_SUSPECTS, columns, null, null, null, null, null);
    }
}
