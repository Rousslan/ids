package ids.policedb_simpleclient.dbutils;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import ids.policedb_simpleclient.StartActivity;

public class DBManager {

    private DBOpenHelper dbOpenHelper;
    private  static DBManager dbManager = new DBManager();

    private DBManager() {
        dbOpenHelper = StartActivity.dbOpenHelper;
    };

    public static DBManager getInstance() {
        return dbManager;
    }

    public void insertIntoDistricts (String district) {
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DBOpenHelper.COLUMN_DISTRICT_NAME, district);
        database.insert(DBOpenHelper.TABLE_DISTRICTS, null, cv);
    }

    public void updateDistrictRow(String[] data){
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DBOpenHelper.COLUMN_DISTRICT_NAME, data[1]);
        database.update(DBOpenHelper.TABLE_DISTRICTS, cv, DBOpenHelper.COLUMN_ID + "= " + data[0], null);
    }

    public void insertIntoOfficers(OfficerDataHolder dataHolder) {
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        database.insert(DBOpenHelper.TABLE_OFFICERS, null, dataHolder.toContentValues());
    }

    public void updateOfficerRow(OfficerDataHolder dataHolder){
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        database.update(DBOpenHelper.TABLE_OFFICERS, dataHolder.toContentValues(), DBOpenHelper.COLUMN_ID + "= " + dataHolder.getId(), null);
    }

    public void insertIntoSuspects(SuspectDataHolder dataHolder) {
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        database.insert(DBOpenHelper.TABLE_SUSPECTS, null, dataHolder.toContentValues());
    }

    public void updateSuspectRow(SuspectDataHolder dataHolder){
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        database.update(DBOpenHelper.TABLE_SUSPECTS, dataHolder.toContentValues(), DBOpenHelper.COLUMN_ID + "= " + dataHolder.getId(), null);
    }

    public void insertIntoArrests(ArrestDataHolder dataHolder) {
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        database.insert(DBOpenHelper.TABLE_ARRESTS, null, dataHolder.toContentValues());
    }

    public void updateArrestRow(ArrestDataHolder dataHolder){
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        database.update(DBOpenHelper.TABLE_ARRESTS, dataHolder.toContentValues(), DBOpenHelper.COLUMN_ID + "= " + dataHolder.getId(), null);
    }

    public void delete (String tableName, int id) {
        dbOpenHelper.getWritableDatabase().delete(tableName, DBOpenHelper.COLUMN_ID + "= " + id, null);
    }
}
