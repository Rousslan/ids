package ids.policedb_simpleclient.dbutils;

import android.database.Cursor;
import android.database.SQLException;

import java.util.ArrayList;
import java.util.List;

import ids.policedb_simpleclient.entities.Arrest;
import ids.policedb_simpleclient.entities.District;
import ids.policedb_simpleclient.entities.Officer;
import ids.policedb_simpleclient.entities.Suspect;

public class DBSelectedData {

    private Cursor districtsCursor;
    private Cursor officersCursor;
    private Cursor suspectsCursor;
    private Cursor arrestsCursor;

    public void setDistrictsCursor(Cursor districtsCursor) {
        this.districtsCursor = districtsCursor;
    }

    public void setOfficersCursor(Cursor officersCursor) {
        this.officersCursor = officersCursor;
    }

    public void setSuspectsCursor(Cursor suspectsCursor) {
        this.suspectsCursor = suspectsCursor;
    }

    public void setArrestsCursor(Cursor arrestsCursor) {
        this.arrestsCursor = arrestsCursor;
    }

    public List<District> getDistricts() throws SQLException {
        if ( (districtsCursor == null) || (districtsCursor.getCount() < 1) ) {
            throw new SQLException("Districts cursor contains no data - setDistrictsCursor() method should be invoked at first!");
        }

        List<District> districts = new ArrayList<>();
        districtsCursor.moveToFirst();
        while (!districtsCursor.isAfterLast()) {
            int id = districtsCursor.getInt(districtsCursor.getColumnIndex(DBOpenHelper.COLUMN_ID));
            String name = districtsCursor.getString(districtsCursor.getColumnIndex(DBOpenHelper.COLUMN_DISTRICT_NAME));
            districts.add(new District(id, name));
            districtsCursor.moveToNext();
        }
        districtsCursor.close();
        return districts;
    }

    public List<Officer> getOfficers() throws SQLException {
        if ( (officersCursor == null) || (officersCursor.getCount() < 1) ) {
            throw new SQLException("Officers cursor contains no data - setOfficersCursor() method should be invoked at first!");
        }

        List<Officer> officers = new ArrayList<>();
        officersCursor.moveToFirst();
        while (!officersCursor.isAfterLast()) {
            Officer officer = new Officer();
            officer.setId(officersCursor.getInt(officersCursor.getColumnIndex(DBOpenHelper.COLUMN_ID)));
            officer.setName(officersCursor.getString(officersCursor.getColumnIndex(DBOpenHelper.COLUMN_NAME)));
            officer.setBirthDate(officersCursor.getString(officersCursor.getColumnIndex(DBOpenHelper.COLUMN_BIRTH_DATE)));
            officer.setAddress(officersCursor.getString(officersCursor.getColumnIndex(DBOpenHelper.COLUMN_ADDRESS)));
            officer.setNumOfArrests(officersCursor.getInt(officersCursor.getColumnIndex(DBOpenHelper.COLUMN_NUMBER_OF_ARRESTS)));
            officers.add(officer);
            officersCursor.moveToNext();
        }
        officersCursor.close();
        return officers;
    }

    public List<Suspect> getSuspects() throws SQLException {
        if ( (suspectsCursor == null) || (suspectsCursor.getCount() <1) ) {
            throw new SQLException("Suspects cursor contains no data - setSuspectsCursor() method should be invoked at first!");
        }

        List<Suspect> suspects = new ArrayList<>();
        suspectsCursor.moveToFirst();
        while (!suspectsCursor.isAfterLast()) {
            Suspect suspect= new Suspect();
            suspect.setId(suspectsCursor.getInt(suspectsCursor.getColumnIndex(DBOpenHelper.COLUMN_ID)));
            suspect.setName(suspectsCursor.getString(suspectsCursor.getColumnIndex(DBOpenHelper.COLUMN_NAME)));
            suspect.setBirthDate(suspectsCursor.getString(suspectsCursor.getColumnIndex(DBOpenHelper.COLUMN_BIRTH_DATE)));
            suspect.setAddress(suspectsCursor.getString(suspectsCursor.getColumnIndex(DBOpenHelper.COLUMN_ADDRESS)));
            suspect.setCrime(suspectsCursor.getString(suspectsCursor.getColumnIndex(DBOpenHelper.COLUMN_CRIME)));

            int conviction = suspectsCursor.getInt(suspectsCursor.getColumnIndex(DBOpenHelper.COLUMN_WAS_CONVICTED));
            boolean wasConvicted = (conviction != 0);
            suspect.setWasConvicted(wasConvicted);
            suspects.add(suspect);
            suspectsCursor.moveToNext();
        }
        suspectsCursor.close();
        return suspects;
    }

    public List<Arrest> getArrests() throws SQLException {
        if ( (arrestsCursor== null) || (arrestsCursor.getCount() < 0) ) {
            throw new SQLException("Arrests  cursor contains no data - setArrestsCursor() method should be invoked at first!");
        }
        List<Arrest> arrests= new ArrayList<>();
        // TODO: get data from Arrests table
        return arrests;
    }

}
