package ids.policedb_simpleclient.adapters;

import android.content.Context;
import android.database.Cursor;

import java.util.HashMap;

import ids.policedb_simpleclient.StartActivity;
import ids.policedb_simpleclient.dbutils.DBOpenHelper;
import ids.policedb_simpleclient.dbutils.OfficersCursorLoader;

public class OfficerFieldAdapter  extends BaseFieldAdapter{

    public OfficerFieldAdapter(Context context) {
        super(context);
    }

    @Override
    protected void loadDataIntoMap() {
        Cursor cursor = new OfficersCursorLoader(getContext(), StartActivity.dbOpenHelper).loadInBackground();
        iDsAndNames = new HashMap<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(cursor.getColumnIndex(DBOpenHelper.COLUMN_ID));
            String name = cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_NAME));
            iDsAndNames.put(id, name);
            cursor.moveToNext();
        }
    }

}
