package ids.policedb_simpleclient.dbutils;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public  class DistrictsCursorLoader extends BaseCursorLoader{

    public DistrictsCursorLoader(Context context, DBOpenHelper dbOpenHelper) {
        super(context, dbOpenHelper);
    }

    @Override
    public Cursor loadInBackground() {
        String[] columns = new String[] {DBOpenHelper.COLUMN_ID, DBOpenHelper.COLUMN_DISTRICT_NAME};
        return getDatabase().query(DBOpenHelper.TABLE_DISTRICTS, columns, null, null, null, null, null);
    }
}
