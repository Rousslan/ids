package ids.policedb_simpleclient;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import ids.policedb_simpleclient.dbutils.DBOpenHelper;
import ids.policedb_simpleclient.fragments.ArrestsFragment;
import ids.policedb_simpleclient.fragments.DistrictsFragment;
import ids.policedb_simpleclient.fragments.OfficersFragment;
import ids.policedb_simpleclient.fragments.StartFragment;
import ids.policedb_simpleclient.fragments.SuspectsFragment;

public class StartActivity extends FragmentActivity{

    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    public static DBOpenHelper dbOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        //setting the start fragment
        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.container, new StartFragment());
        transaction.commit();
        dbOpenHelper = new DBOpenHelper(this);

//        getApplicationContext().deleteDatabase(DBOpenHelper.DATABASE_NAME);
    }

    public void handleTableChoice(View view) {
        switch (view.getId()) {
            case R.id.districts:
                showDistricts();
                break;
            case R.id.officers:
                showOfficers();
                break;
            case R.id.suspects:
                showSuspects();
                break;
            case R.id.arrests:
                showArrests();
        }
    }

    private void showDistricts() {
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, new DistrictsFragment());
        transaction.addToBackStack("districts");
        transaction.commit();
    }

    private void showOfficers() {
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, new OfficersFragment());
        transaction.addToBackStack("officers");
        transaction.commit();
    }

    private void showSuspects() {
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, new SuspectsFragment());
        transaction.addToBackStack("suspects");
        transaction.commit();

    }

    private void showArrests() {
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, new ArrestsFragment());
        transaction.addToBackStack("arrests");
        transaction.commit();
    }

    }