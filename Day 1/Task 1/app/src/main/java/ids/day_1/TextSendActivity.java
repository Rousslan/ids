package ids.day_1;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TextSendActivity extends ActionBarActivity {

    private TextView enterText;
    private EditText textToSend;
    Button toStartActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_send);

        enterText = (TextView) findViewById(R.id.textView);
        textToSend = (EditText) findViewById(R.id.textToSend);
        toStartActivity = (Button) findViewById(R.id.toStart);
    }

    /*get back to StartActivity sending
     *the text entered in the EditText*/
    public void backToStartActivity(View view) {
        Intent intent = new Intent();
        intent.putExtra("textToSend", textToSend.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.text_send, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
