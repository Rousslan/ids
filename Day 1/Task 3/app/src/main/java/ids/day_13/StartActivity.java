package ids.day_13;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class StartActivity extends ActionBarActivity {

    private int value;
    private TextView valueLabel;
    private Button inc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        valueLabel = (TextView) findViewById(R.id.valueLbl);
        inc = (Button) findViewById(R.id.incButton);
        inc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valueLabel.setText("" + ++value);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("value", value);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        value = savedInstanceState.getInt("value");
        valueLabel.setText("" + value);
    }
}
