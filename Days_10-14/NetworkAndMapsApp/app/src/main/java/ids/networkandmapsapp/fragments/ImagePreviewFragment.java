package ids.networkandmapsapp.fragments;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import ids.networkandmapsapp.R;
import ids.networkandmapsapp.dbutils.DBManager;
import ids.networkandmapsapp.dbutils.DBOpenHelper;
import ids.networkandmapsapp.dbutils.PhotoDataHolder;

public class ImagePreviewFragment extends Fragment
                                                                                    implements LoaderManager.LoaderCallbacks<Cursor> {

    private ImageView photo;
    private Button upload;
    private String pathToPhoto;
    private PhotoDataHolder dataHolder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_preview, container, false);

        photo = (ImageView) view.findViewById(R.id.photo);
        upload = (Button) view.findViewById(R.id.upload);

        final String PHOTO_ID_IN_DB = "photoID";
        int  id = getArguments().getInt(PHOTO_ID_IN_DB);

        Bundle bundle = new Bundle();
        final String ID = "id";
        final int PHOTO_LOADER_ID = 1;
        bundle.putInt(ID, id);
        getActivity().getSupportLoaderManager().initLoader(PHOTO_LOADER_ID, bundle, this).forceLoad();

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadPhoto();
            }
        });
        return view;
    }

    private void uploadPhoto() {
        ConnectivityManager connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new Thread(new PhotoUpload()).start();
        } else {
            Toast.makeText(getActivity().getApplicationContext(), "Failed to establish connection!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, final Bundle bundle) {
        Context context = getActivity().getApplicationContext();
        return new AsyncTaskLoader<Cursor>(context) {
            @Override
            public Cursor loadInBackground() {
                final String ID = "id";
                return DBManager.getInstance().getPhotoDataFromDB(bundle.getInt(ID));
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor c) {
        PhotoDataHolder dataHolder = new PhotoDataHolder();
        if (c != null) {
            if (c.moveToFirst()) {
                dataHolder.setPath(c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_PATH)));
                dataHolder.setLatitude(Double.valueOf(c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_LATITUDE))));
                dataHolder.setLongitude(Double.valueOf(c.getString(c.getColumnIndex(DBOpenHelper.COLUMN_LONGITUDE))));
                boolean u = c.getInt(c.getColumnIndex(DBOpenHelper.COLUMN_IS_UPLOAD)) == 1;
                dataHolder.setUpload(u);
            }
            c.close();
            pathToPhoto = dataHolder.getPath();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(pathToPhoto, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;
            String imageType = options.outMimeType;
            // TODO: resize the photo according to imageview's size

            Uri uri = Uri.fromFile(new File(pathToPhoto));
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                photo.setImageBitmap(bitmap);
            } catch (IOException e) {
                Toast.makeText(getActivity().getApplicationContext(), "Failed to show the photo", Toast.LENGTH_LONG).show();
            }
            upload.setEnabled(!dataHolder.isUpload());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private class PhotoUpload implements Runnable {

        private  final String URL = "https://api.parse.com/1/files/" + pathToPhoto.substring(pathToPhoto.lastIndexOf("/"));
        private String uploadStatusMsg;
        private boolean isSuccessfulUpload;

        @Override
        public void run() {
            try {
                doUploading();
                DBManager.getInstance().updateUploadStatus(pathToPhoto, true);
                isSuccessfulUpload = true;
                uploadStatusMsg = "The photo is successfully uploaded!";
            } catch (IOException e) {
                uploadStatusMsg = "An error occurred while uploading the photo! ";
                isSuccessfulUpload = false;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity().getApplicationContext(), uploadStatusMsg, Toast.LENGTH_LONG).show();
                    upload.setEnabled( !isSuccessfulUpload);
                }
            });
        }

        private void doUploading() throws IOException {
            HttpsURLConnection connection = getHttpURLConnection();
            OutputStream os = connection.getOutputStream();
            byte[] buffer = new byte[1024];
            ByteArrayInputStream is = new ByteArrayInputStream(getBytesFromPhoto());
            while ((is.read(buffer) != -1)) {
                os.write(buffer);
            }
            os.flush();
            os.close();
            /*int i = connection.getResponseCode();
            String s = connection.getResponseMessage();
            Map<String, List<String>> map = connection.getHeaderFields();
            String location = map.get("Location").get(0);*/
            Log.d("Response", connection.getResponseCode()+"");
            connection.disconnect();
        }

        private byte[] getBytesFromPhoto() throws IOException {
            Uri uri = Uri.fromFile(new File(pathToPhoto));
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            return stream.toByteArray();
        }

        private HttpsURLConnection getHttpURLConnection() throws IOException {
            URL url = new URL(URL);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.addRequestProperty("X-Parse-Application-Id", "g8IXYtFHgj3JZNgq53fxiGKfKY8PfR0e8vNIRaaR");
            connection.addRequestProperty("X-Parse-REST-API-Key", "vJpOWFCdcevgAm1BLSpqXZftsEkCtNBWlzxcP81l");
            connection.addRequestProperty("Content-Type", "image/jpeg");
            connection.connect();
            return connection;
        }
    }

}
