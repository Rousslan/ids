package ids.networkandmapsapp.dbutils;

import android.content.ContentValues;

public class PhotoDataHolder {

    private int id;
    private String path;
    private double latitude;
    private double longitude;
    private boolean isUpload;

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DBOpenHelper.COLUMN_PATH, path);
        cv.put(DBOpenHelper.COLUMN_LONGITUDE, longitude);
        cv.put(DBOpenHelper.COLUMN_LATITUDE, latitude);
        cv.put(DBOpenHelper.COLUMN_IS_UPLOAD, isUpload);
        return cv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double lattitude) {
        this.latitude = lattitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isUpload() {
        return isUpload;
    }

    public void setUpload(boolean isUpload) {
        this.isUpload = isUpload;
    }
}
