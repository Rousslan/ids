package ids.networkandmapsapp;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import ids.networkandmapsapp.dbutils.DBOpenHelper;
import ids.networkandmapsapp.fragments.MainFragment;


public class StartActivity extends ActionBarActivity {

    public static  DBOpenHelper dbOpenHelper ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.container, new MainFragment());
        transaction.commit();

        dbOpenHelper  = new DBOpenHelper(getApplicationContext());
        /*getApplicationContext().deleteDatabase(DBOpenHelper.DATABASE_NAME);*/
    }

}
