package ids.networkandmapsapp.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;

import ids.networkandmapsapp.R;
import ids.networkandmapsapp.StartActivity;
import ids.networkandmapsapp.dbutils.DBOpenHelper;

public class AllImagesPreviewFragment extends ListFragment
                                                                                             implements LoaderManager.LoaderCallbacks<Cursor>{

    private static int PATH_LOADER = 1;
    private List<String> photoPaths;
    private ImageLoader imageLoader;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity().getApplicationContext()));

        getActivity().getSupportLoaderManager().initLoader(PATH_LOADER, null, this).forceLoad();
    }

    /*    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_images_preview, container, false);

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity().getApplicationContext()));

        getActivity().getSupportLoaderManager().initLoader(PATH_LOADER, null, this).forceLoad();
        return view;
    }*/

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Context context = getActivity().getApplicationContext();
        return new AsyncTaskLoader<Cursor>(context) {
            @Override
            public Cursor loadInBackground() {
                final String GET_PHOTO_PATHS = "SELECT " + DBOpenHelper.COLUMN_PATH + " FROM " + DBOpenHelper.TABLE_PHOTOS;
                return StartActivity.dbOpenHelper.getReadableDatabase().rawQuery(GET_PHOTO_PATHS, null);
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        photoPaths = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    photoPaths.add(cursor.getString(cursor.getColumnIndex(DBOpenHelper.COLUMN_PATH)));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        PhotosArrayAdapter adapter = new PhotosArrayAdapter();
        setListAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) { }

    class PhotosArrayAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return photoPaths.size();
        }

        @Override
        public Object getItem(int position) {
            return photoPaths.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.images_list_icon_view, parent,false);
            }


            imageLoader.displayImage(photoPaths.get(position), (ImageView) view.findViewById(R.id.thumbnail));
            String name = photoPaths.get(position).substring(photoPaths.get(position).lastIndexOf("/") + 1);
            ((TextView)view.findViewById(R.id.name)).setText(name);
            return view;
        }
    }
}
