package ids.networkandmapsapp.fragments;

import android.app.Activity;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ids.networkandmapsapp.R;
import ids.networkandmapsapp.dbutils.DBManager;
import ids.networkandmapsapp.dbutils.DBOpenHelper;
import ids.networkandmapsapp.dbutils.PhotoDataHolder;

public class MainFragment extends Fragment {

    private static final int IMAGE_REQUEST_CODE = 1;
    /*private Button takePhoto;
    private Button browsePhotos;
    private Button browseMaps;*/
    private File photoFile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        Button takePhoto = (Button) view.findViewById(R.id.takePhoto);
        Button browsePhotos = (Button) view.findViewById(R.id.browsePhotos);
        Button browseMaps = (Button) view.findViewById(R.id.browseMaps);

        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeAPhotoWithCamera();
            }
        });
        browsePhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browseAllPhotosInList();
            }
        });

        return view;
    }

    public void takeAPhotoWithCamera() {
        Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (photoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            try {
                photoFile = createPhotoFile();
            } catch (IOException ex) {
                Toast.makeText(getActivity().getApplicationContext(), "Failed to create file for the photo - the photo will not be saved!", Toast.LENGTH_LONG).show();
                return;
            }
            photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            startActivityForResult(photoIntent, IMAGE_REQUEST_CODE);
        }
    }

    private File createPhotoFile() throws IOException {
        String timeOfPhotoTaking = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String photoFileName = "Photo_" + timeOfPhotoTaking;
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile( photoFileName,  ".jpg", storageDir );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            int id = savePhotoDataToDB();
            final String PHOTO_ID_IN_DB = "photoID";
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            ImagePreviewFragment previewFragment = new ImagePreviewFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(PHOTO_ID_IN_DB, id);
            previewFragment.setArguments(bundle);
            transaction.replace(R.id.container, previewFragment);
            transaction.addToBackStack("preview");
            transaction.commit();
        }
    }

    private int savePhotoDataToDB() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setCostAllowed(false);
        String provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);
        PhotoDataHolder  dataHolder = new PhotoDataHolder();
        dataHolder.setPath(photoFile.getAbsolutePath());
        if (location == null) {
                /*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);*/

            /*Added in order to test app's work on Genymotion*/
            dataHolder.setLatitude(48.1423);
            dataHolder.setLongitude(37.3037);
            return DBManager.getInstance().insertDataIntoPhotos(dataHolder);
        }
        dataHolder.setLongitude(location.getLongitude());
        dataHolder.setLatitude(location.getLatitude());
        return DBManager.getInstance().insertDataIntoPhotos(dataHolder);
    }


    private void browseAllPhotosInList() {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, new AllImagesPreviewFragment());
        transaction.addToBackStack("allImages");
        transaction.commit();
    }

}
