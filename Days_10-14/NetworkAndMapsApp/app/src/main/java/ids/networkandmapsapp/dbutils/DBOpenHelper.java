package ids.networkandmapsapp.dbutils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DBOpenHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "photodata";
    // Table name
    public static final String TABLE_PHOTOS= "photos";
    // id constant
    public static final String COLUMN_ID  = "_id";
    // Photos' table's column names
    public static final String COLUMN_PATH = "ImagePath";
    public static final String COLUMN_LONGITUDE = "Longitude";
    public static final String COLUMN_LATITUDE = "Latitude";
    public static final String COLUMN_IS_UPLOAD = "IsUpload";

    private Context context;

    public  DBOpenHelper(Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String CREATE_PHOTOS_TABLE = "CREATE TABLE " + TABLE_PHOTOS +
                                                                               "( " + COLUMN_ID + " INTEGER PRIMARY KEY,  "
                                                                                       +COLUMN_PATH + " TEXT, "
                                                                                       + COLUMN_LONGITUDE+ " TEXT, "
                                                                                       + COLUMN_LATITUDE + " TEXT, "
                                                                                       + COLUMN_IS_UPLOAD + " INTEGER " + ")";
        database.execSQL(CREATE_PHOTOS_TABLE);
        /*Toast.makeText(context, "DB for storing images info is created!", Toast.LENGTH_SHORT).show();*/
        Log.i( "DB created", "DB for images info storing is created!");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i2) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_PHOTOS);
        onCreate(database);
    }

}
