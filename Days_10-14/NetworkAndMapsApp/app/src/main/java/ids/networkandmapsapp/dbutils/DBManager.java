package ids.networkandmapsapp.dbutils;

import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import ids.networkandmapsapp.StartActivity;

public class DBManager {

    private DBOpenHelper dbOpenHelper ;
    private  static DBManager dbManager = new DBManager();

    private DBManager() {
        dbOpenHelper = StartActivity.dbOpenHelper;
    };

    public static DBManager getInstance() {
        return dbManager;
    }

    public int insertDataIntoPhotos(PhotoDataHolder dataHolder) {
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        ContentValues cv = dataHolder.toContentValues();
       return (int)database.insert(DBOpenHelper.TABLE_PHOTOS, null, cv);
    }

    public Cursor  getPhotoDataFromDB(int id) {
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();
        return database.query(DBOpenHelper.TABLE_PHOTOS, null, DBOpenHelper.COLUMN_ID + " = " + id, null, null, null, null);
    }

    public void updateUploadStatus(String pathToPhoto, boolean isUpload) {
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DBOpenHelper.COLUMN_IS_UPLOAD, isUpload);
        database.update(DBOpenHelper.TABLE_PHOTOS, cv, DBOpenHelper.COLUMN_PATH + "= " + "\"" + pathToPhoto + "\"", null );
    }

    public boolean isPhotoUploaded (String pathToPhoto) {
        final String GET_UPLOAD_STATUS_BY_PATH_TO_PHOTO = "SELECT " + DBOpenHelper.COLUMN_IS_UPLOAD+
                                                                                                                                  " FROM " + DBOpenHelper.TABLE_PHOTOS +
                                                                                                                                  " WHERE " + DBOpenHelper.COLUMN_PATH + " = " + pathToPhoto;
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();
        Cursor c = database.rawQuery(GET_UPLOAD_STATUS_BY_PATH_TO_PHOTO, null);
        int upload = 0;
        if (c != null) {
            if (c.moveToFirst()) {
                upload = c.getInt(c.getColumnIndex(DBOpenHelper.COLUMN_IS_UPLOAD));
            }
            c.close();
        }
        return (upload == 1);
    }

}

