package ids.task_41;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class UpperFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_upper, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        Button takeTextToTextView = (Button) getActivity().findViewById(R.id.takeTextToTextView);
        takeTextToTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText textToTake = (EditText) getActivity().findViewById(R.id.textToTake);
                TextView text = (TextView) getActivity().findViewById(R.id.text);
                text.setText(textToTake.getText().toString());
            }
        });
    }
}
