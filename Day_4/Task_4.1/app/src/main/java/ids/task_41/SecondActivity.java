package ids.task_41;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


public class SecondActivity extends FragmentActivity {

    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        fragmentManager = getSupportFragmentManager();
        initFragments();
    }

    private void initFragments() {
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.containerUp , new UpperFragment());
        transaction.add(R.id.containerLow , new LowerFragment());
        transaction.commit();
    }


}
