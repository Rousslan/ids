package ids.task_42;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import ids.task_42.fragments.FirstFragment;
import ids.task_42.fragments.FourthFragment;
import ids.task_42.fragments.SecondFragment;
import ids.task_42.fragments.ThirdFragment;


public class StartActivity extends FragmentActivity {

    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        fragmentManager = getSupportFragmentManager();
        initFragment();

    }

    private void initFragment() {
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.container , new FirstFragment());
        //transaction.addToBackStack("frag1");
        transaction.commit();
    }

    public void buttonsClicked (View view) {
        switch (view.getId()) {
            case R.id.btn1:
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.container, new SecondFragment());
                transaction.addToBackStack("frag2");
                transaction.commit();
                break;
            case R.id.btn2:
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.container, new ThirdFragment());
                transaction.addToBackStack("frag3");
                transaction.commit();
                break;
            case R.id.btn3:
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.container, new FourthFragment());
                transaction.addToBackStack("frag4");
                transaction.commit();
                break;
            case R.id.btn4:
                transaction = fragmentManager.beginTransaction();
                fragmentManager.popBackStack("frag2", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                transaction.commit();
                break;
        }
    }

}
