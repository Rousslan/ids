package ids.task_43;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

import ids.task_43.fragments.FirstFragment;


public class StartActivity extends FragmentActivity {

    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        ViewPager pager = (ViewPager) findViewById(R.id.viewPager);
        pagerAdapter = new SimplePagerAdapter(getSupportFragmentManager(), createFragments());
        pager.setAdapter(pagerAdapter);
    }

    private List<Fragment> createFragments() {
        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(FirstFragment.newInstance("Fragment 1", 0xffd4e619));
        fragments.add(FirstFragment.newInstance("Fragment 2", 0xff51e609));
        fragments.add(FirstFragment.newInstance("Fragment 3", 0xff2cc4e6));
        fragments.add(FirstFragment.newInstance("Fragment 4", 0xfff168a1));
        fragments.add(FirstFragment.newInstance("Fragment 5", 0xfffadcf0));

        return fragments;
    }


    private class SimplePagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragments;

        public SimplePagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int i) {
            return fragments.get(i);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
