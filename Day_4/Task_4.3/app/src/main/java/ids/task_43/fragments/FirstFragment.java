package ids.task_43.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import ids.task_43.R;

public class FirstFragment extends Fragment {

    private static final String COLOR_VALUE = "colorValue";
    private static final String FRAGMENT_NAME = "fragmentName";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        FrameLayout containerLayout = (FrameLayout) view.findViewById(R.id.container);
        containerLayout.setBackgroundColor(getArguments().getInt(COLOR_VALUE));

        TextView fragmentName = (TextView) view.findViewById(R.id.frag_name);
        fragmentName.setText(getArguments().getString(FRAGMENT_NAME).toString());

        return view;
    }

    public static FirstFragment newInstance(String text, Integer color) {
        FirstFragment fragment = new FirstFragment();
        Bundle b = new Bundle();
        b.putString(FRAGMENT_NAME, text);
        b.putInt(COLOR_VALUE, color);

        fragment.setArguments(b);

        return fragment;
    }


}
