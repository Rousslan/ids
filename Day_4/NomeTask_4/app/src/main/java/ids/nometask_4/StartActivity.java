package ids.nometask_4;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class StartActivity extends ActionBarActivity {

    private String GOOGLE_HTML_CODE = "googleHtmlCode";
    private Button goToDownload;
    private EditText htmlText;

    private final int DOWNLOAD_RESULT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        htmlText = (EditText) findViewById(R.id.htmlText);
        goToDownload = (Button) findViewById(R.id.goToDownload);
        goToDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent downloadIntent = new Intent(getApplicationContext(), GoogleCodeDownloaderActivity.class);
                startActivityForResult(downloadIntent, DOWNLOAD_RESULT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case DOWNLOAD_RESULT:
                if (resultCode == RESULT_OK) {
                    htmlText.setText(data.getStringExtra(GOOGLE_HTML_CODE));
                }
        }
    }
}
