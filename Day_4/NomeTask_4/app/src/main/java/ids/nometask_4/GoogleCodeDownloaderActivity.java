package ids.nometask_4;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class GoogleCodeDownloaderActivity extends ActionBarActivity {

    private String GOOGLE_HTML_CODE = "googleHtmlCode";
    private Button downloadCode;
    private ProgressDialog status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_code_downloader);

        downloadCode = (Button) findViewById(R.id.downloadCode);
        downloadCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CodeDownloadTask downloadTask = new CodeDownloadTask();
                downloadTask.execute();
            }
        });
    }

    private class CodeDownloadTask extends AsyncTask<Void, Void, String> {

        private final String GOOGLE_URL = "http://google.com";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            status = ProgressDialog.show(GoogleCodeDownloaderActivity.this, "","Downloading HTML code..", true);
            status.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            StringBuilder htmlCode = new StringBuilder();
            URL url;
            InputStream stream = null;
            BufferedReader reader;

            try {
                url = new URL(GOOGLE_URL);
                stream = url.openStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                String line;
                while ((line = reader.readLine()) != null) {
                    htmlCode.append(line);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (stream != null) {
                        stream.close();
                    }
                } catch (IOException ioe) {
                    //nothing to do here
                }
            }
           return htmlCode.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            status.dismiss();
            Intent intent = new Intent();
            intent.putExtra(GOOGLE_HTML_CODE, s);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
